# Girocco::TimedToken.pm -- HMAC Timed Token Utility Functions
# Copyright (C) 2021 Kyle J. McKay.  All rights reserved.

# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.

package Girocco::TimedToken;

use strict;
use warnings;

use Scalar::Util qw(refaddr looks_like_number);
use MIME::Base64;
use Digest::MD5 qw(md5);
BEGIN {
	eval {
		require Digest::SHA;
		Digest::SHA->import(
			qw(sha1)
	);1} ||
	eval {
		require Digest::SHA1;
		Digest::SHA1->import(
			qw(sha1)
	);1} ||
	eval {
		require Digest::SHA::PurePerl;
		Digest::SHA::PurePerl->import(
			qw(sha1)
	);1} ||
	die "One of Digest::SHA or Digest::SHA1 or Digest::SHA::PurePerl "
	. "must be available\n";
}
use Girocco::HashUtil qw(hmac_sha1);

use base qw(Exporter);
our @EXPORT;
our $VERSION;

BEGIN {
	@EXPORT = qw(create_timed_token verify_timed_token create_token_secret);
	*VERSION = \'1.0';
}

# Like MIME::Base64::encode_base64 except that the base64url alphabet is used
# and no newlines are ever added
sub _encode_base64_url {
	use bytes;
	my $b64 = encode_base64($_[0], "");
	# convert standard base64 encoding to the base64url encoding
	$b64 =~ tr{+/}{-_};
	return $b64;
}

# Like MIME::Base64::decode_base64 except that the base64url alphabet is used
# Same arguments as MIME::Base64::decode_base64
sub _decode_base64_url {
	use bytes;
	my $b64 = shift;
	# convert base64url encoding to the standard base64 encoding
	$b64 =~ tr{-_}{+/};
	return decode_base64($b64);
}

# Return a new "robust" secret value that can be passed to
# create_timed_token as $_[0].
#
# Although any value can be used as a secret, this function
# attempts to generate a decently random one with sufficient
# entropy to avoid collision attacks.
#
# These are meant to remain "secret" -- any client that manages
# to obtain the secret will likely be able to forge tokens.
#
# Since new token secrets are only expected to be created once
# in a while and certainly not on every single request, it's
# not necessary for this function to be instantaneous.
#
# Note that even though the returned result is in base64url
# format, it should be passed as-is to create_timed_token
# and verify_timed_token -- there's absolutely no need to
# base64url decode it first since it's already guaranteed
# to have more than enough bits of entropy in its much more
# convenient base64url form.
#
# Note that although rare, the output of this function can
# start with '-' or with '_' rather than an alphanumeric.
# Make sure it can never be accidentally treated as an option
# argument to some utility.
#
sub create_token_secret {
	# We would like to produce at least 20 bytes worth
	# of entropy, but returned encoded as base64url for
	# convenience in loading and storing.
	# To further mash things up, after collecting enough
	# random (pseudo-random really) raw bits, pass those
	# through two different hash functions, concatenate the
	# result and then base64url encode it.
	# Using cryptographic quality hash functions will end
	# up reducing the number of entropy bits to approximately
	# 63.212% of the number we started with.  Therefore we don't
	# just collect 160 bits (20 * 8), we collect 160 / 0.63212
	# which we simply round up to 256 bits (32 bytes).
	#
	# We take input from /dev/urandom if it exists and is
	# readable (or /dev/random in it's place if that is),
	# Then add a bunch of rand() results and the few bits
	# of truly random information available (the pid, the
	# current time, etc.).  In this way, even if /dev/urandom
	# (or /dev/random) is not available, we can still produce
	# enough stuff to feed through the hash functions to
	# get a decent secret.
	#
	use bytes;
	my $devrand = undef;
	-c '/dev/urandom' && -r _ and $devrand = '/dev/urandom';
	!$devrand && -c '/dev/random' && -r _ and $devrand = '/dev/random';
	my $input = "";
	if ($devrand) {
		# Best source, so always get enough from here
		# if this source is available.
		my $drh;
		if (open($drh, '<', $devrand)) {
			binmode($drh);
			while (length($input) < 32) {
				last unless sysread($drh, $input,
					32-length($input), length($input));
			}
			close($drh);
		}
	}
	for (my $w = 0; $w < 32; $w += 2) {
		# use rand to get 16 bits at a time
		$input .= pack('n',int(rand(32768)));
	}
	# Glue on pid and current time
	$input .= $$ . time();
	# And just for kicks, the last mod time of this file
	my $mod = (stat(__FILE__))[9];
	defined($mod) and $input .= $mod;
	# And finally the address of this routine
	my $ra = refaddr(\&create_token_secret);
	defined($ra) and $input .= $ra;
	my $h = sha1($input) . md5($input);
	return _encode_base64_url($h);
}

# $_[0] -> "secret" to use for HMAC
# $_[1] -> optional instance info to include in "text"
# $_[2] -> duration of validity in seconds (5..2147483647)
# $_[3] -> optional time stamp (secs since unix Epoch)
#          if not provided, current time is used
# Returns a base64_url token (no trailing '='s) that is
# valid starting at $_[3] and expires $_[2] seconds after $_[3].
#
sub create_timed_token {
	use bytes;
	my $t = $_[3];
	looks_like_number($t) or $t = time();
	$t = int($t);
	my $d = int($_[2]);
	5 <= $d && $d <= 2147483647 or
		die "crazy create_timed_token duration: $d";
	my $tp = int($t / $d);
	my $to = int($t - ($tp * $d));
	my $raw = _get_raw_hmac($_[0], $_[1], $tp, $to);
	if ($to <= 255) {
		$raw .= pack('C', $to);
	} else {
		$raw .= pack('N', $to);
	}
	return _encode_base64_url($raw);
}

# $_[0] -> a create_timed_token to verify
# $_[1] -> "secret" passed to create_timed_token
# $_[2] -> instance info passed to create_timed_token
# $_[3] -> validity in seconds passed to create_timed_token
# $_[4] -> optional time stamp (secs since unix Epoch)
#          if not provided, current time is used
# Returns true if $_[4] falls within the token's validity range
# Returns false for a bad or expired token
#
# Forging a token would require knowing the arguments that were
# passed to create_timed_token to create it or the ability to
# generate cryptographic HMAC collisions.
#
sub verify_timed_token {
	use bytes;
	my $tok = shift;
	# shortest possible token is 28 base64url characters
	# maximum (if an extremely long period is used) is 32.
	defined($tok) && $tok =~/^[A-Za-z0-9_-]{28,32}$/ or
		return undef;
	# and, in fact, in-between lengths are incorrect too
	length($tok) == 28 || length($tok) == 32 or
		return undef;
	my $raw = _decode_base64_url($tok);
	defined($raw) && (length($raw) == 21 || length($raw) == 24) or do {
		warn "_decode_base64_url failed to decode properly";
		return undef;
	};
	my $h = substr($raw, 0, 20);
	my $o;
	if (length($raw) == 21) {
		$o = unpack('C', substr($raw, 20, 1));
	} else {
		$o = unpack('N', substr($raw, 20, 4));
	}
	defined($o) or do {
		warn "failed to unpack offset somehow";
		return undef;
	};
	$o = int($o);
	my $t = $_[3];
	looks_like_number($t) or $t = time();
	$t = int($t);
	my $tp = int($t / int($_[2]));
	my $test = _get_raw_hmac($_[0], $_[1], $tp, $o);
	if ($test ne $h) {
		--$tp;
		$test = _get_raw_hmac($_[0], $_[1], $tp, $o);
		$test eq $h or return undef;  # definitely no match
	}
	my $tokbegin = ($tp * int($_[2])) + $o;
	return $tokbegin <= $t && $t < ($tokbegin + int($_[2]));
}

sub _get_raw_hmac {
	my ($secret, $extra, $period, $offset) = @_;
	$period = int($period);
	$offset = int($offset);
	my $text = '$TimedToken$'.$extra.'$'.$period.'$'.$offset;
	return hmac_sha1($text, $secret);
}

1;
