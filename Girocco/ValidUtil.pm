# This package contains validation checks that are use'd by
# both Girocco::Validator (which is an install-only module)
# and by Girocco::Util (which is a runtime module).
# It MUST NOT use any other Girocco modules!!!
# (And really SHOULD NOT use any other modules either.)

package Girocco::ValidUtil;

use 5.008;
use strict;
use warnings;

BEGIN {
	use base qw(Exporter);
	our @EXPORT = qw(valid_branch_name);
}

# Return undef if input argument is undef
# Otherwise return true if the argument is considered a valid Git branch name
# as far as Girocco is concerned.  The "branch name" is the part AFTER the
# initial "refs/heads/".  In other words the ref "refs/heads/master" represents
# the "branch name" known as "master".
# This function expects the bare branch name (e.g. "master") to be passed to
# it for validation.  Note that an empty branch name (i.e. "") is not valid.
# Branch naming rules (mostly the rules applied by `git check-ref-format` with
# a few Girocco extra restrictions thrown in):
#  1. MUST NOT start with or end with a '/'(0x2f)
#  2. MAY have hierarchical components which MUST be separated with '/'(0x2f)
#  3. MUST NOT have any empty components (the part between two '/'s) (i.e. '//')
#  4. MUST NOT have any component that starts with or ends with a '.'(0x2e)
#  5. MUST NOT have any component that ends with '.lock' (case-insensitively)
#  6. MUST NOT contain any characters with a value between 0x00 and 0x1f (inclusive)
#  7. MUST NOT contain any space (0x20), backslash (0x5c) or DEL (0x7f) characters
#  8. MUST NOT contain any globbing characters '?'(0x3f), '*'(0x2a), '['(0x5b)
#  9. MUST NOT contain any Git-special characters '~'(0x7e), '^'(0x5e), ':'(0x3a)
# 10. MUST NOT contain the Git-special sequence '..'(0x2e 0x2e)
# 11. MUST NOT contain the Git-special sequence '@{'(0x40 0x7b)
# 12. MUST NOT be the Git-special branch name '@'(0x40)
# 13. MUST NOT contain (Girocco-disallowed) any quote characters "'"(0x27) or '"'(0x22)
# 14. MUST NOT contain (Girocco-disallowed) any '<'(0x3c) or '>'(0x3e) characters
sub valid_branch_name {
	my $b = shift;
	defined $b or return undef;
	$b ne "" or return "";
	return $b !~ m{
		^/ | /$ |				# rule 1
		// |					# rules 2+3
		(?:^|/)[.] | [.](?:/|$) |		# rule 4
		[.][lL][oO][cC][kK](?:/|$) |		# rule 5
		[\x00-\x1f "'"*:<>?\[\\^~\x7f] |	# rules 6+7+8+9+13+14
		[.][.] | [\@][\{] |			# rules 10+11
		^\@$					# rule 12
	}xo;
}

1;
