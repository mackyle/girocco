# This install-only package contains validation, sanity and default
# checks for the values set in Girocco::Config.

# It's only used during the install process.
# Normally the Girocco::Config file will "require" this module
# at the end (before it's frozen during installation).
# However, the install process specifically "use"s this module to
# guarantee the checks always run even if that "require" is removed.

package Girocco::Validator;

BEGIN {
	# This check MUST NOT be inside the Girocco::Config package
	scalar(eval 'keys %Girocco::Config::') or
		die "Girocco::Config must already be 'use'd before ".__PACKAGE__." is 'use'd.\n";
}

use Girocco::ValidUtil ();

package Girocco::Config;

use strict;
use warnings;
no strict 'vars'; # required since Config.pm declares the variables

BEGIN {
	# Makes non-Config modules work with non-default Config
	exists($INC{'Girocco/Config.pm'}) or
		$INC{'Girocco/Config.pm'} = $INC{'Girocco/Validator.pm'};
}

#
##  ------------------------
##  Sanity checks & defaults
##  ------------------------
#
#  Changing anything in this section can result in unexpected breakage

# Couple of sanity checks and default settings (do not change these)
require Digest::MD5;
require MIME::Base64;
defined($name) or $name = "";
$name =~ s/\s+/_/gs;
$nickname = lc((split(/[.]/, $name))[0]) unless defined($nickname) && $nickname ne "";
$nickname =~ s/\s+/_/gs;
our $tmpsuffix = substr(MIME::Base64::encode_base64(Digest::MD5::md5($name.':'.$nickname)),0,6);
$tmpsuffix =~ tr,+/,=_,;
defined($mirror_user) && $mirror_user ne "" or
	die "Girocco::Config: \$mirror_user must be set even if to current user";
defined($basedir) && $basedir ne "" or
	die "Girocco::Config: \$basedir must be set";
defined($sendmail_bin) && $sendmail_bin ne "" or
	die "Girocco::Config: \$sendmail_bin must be set";
$sendmail_bin eq "sendmail.pl" and $sendmail_bin = "$basedir/bin/sendmail.pl";
defined($screen_acl_file) && $screen_acl_file ne "" or
	$screen_acl_file = "$basedir/screen/giroccoacl";
defined($jailreporoot) or $jailreporoot = "";
$jailreporoot =~ s,^/+,,;
$reporoot ne "" or die "Girocco::Config: \$reporoot must be set";
$jailreporoot ne "" or die "Girocco::Config: \$jailreporoot must be set";
$disable_jailsetup = $disable_jailsetup ? 1 : '';
$notify_single_level = $notify_single_level ? 1 : '';
$fetch_stash_refs = $fetch_stash_refs ? 1 : '';
!$mob || $mob eq 'mob' or die "Girocco::Config: \$mob must be undef (or '') or 'mob'";
!defined($protect_fields) || ref($protect_fields) eq 'HASH' or
	die "Girocco::Config: \$protect_fields must be a HASH ref or undefined";
ref($protect_fields) eq 'HASH' or $protect_fields = {};
$project_edit_timeout =~ /^[1-9][0-9]*$/ or
	die "Girocco::Config: \$project_edit_timeout must be a positive integer";
5 <= $project_edit_timeout && $project_edit_timeout <= 86400 or
	die "Girocco::Config: \$project_edit_timeout seems unreasonable: $project_edit_timeout";
!$min_key_length || $min_key_length =~ /^[1-9][0-9]*$/ or
	die "Girocco::Config: \$min_key_length must be undef or numeric";
!defined($max_readme_size) || $max_readme_size =~ /^[0-9]+$/ or
	die "Girocco::Config: \$max_readme_size must be a whole number";
defined($mailsh_sizelimit) && $mailsh_sizelimit =~ /^[1-9][0-9]*$/ or
	die "Girocco::Config: \$mailsh_sizelimit must be a positive number";
!defined($initial_branch) || $initial_branch eq "" ||
    Girocco::ValidUtil::valid_branch_name($initial_branch) or
	die "Girocco::Config: \$initial_branch grossly invalid: $initial_branch";
if (defined($empty_commit_message)) {
	$empty_commit_message =~ s/^\s+//;
	$empty_commit_message =~ s/\s+$//;
}
$admincc = $admincc ? 1 : 0;
$rootcert = "$certsdir/girocco_root_crt.pem" if $httpspushurl && !$rootcert;
$clientcert = "$certsdir/girocco_client_crt.pem" if $httpspushurl && !$clientcert;
$clientkey = "$certsdir/girocco_client_key.pem" if $httpspushurl && !$clientkey;
$clientcertsuffix = "$certsdir/girocco_client_suffix.pem" if $httpspushurl && !$clientcertsuffix;
$mobusercert = "$certsdir/girocco_mob_user_crt.pem" if $httpspushurl && $mob && !$mobusercert;
$mobuserkey = "$certsdir/girocco_mob_user_key.pem" if $httpspushurl && $mob && !$mobuserkey;
our $mobpushurl = $pushurl;
$mobpushurl =~ s,^ssh://,ssh://mob@,i if $mobpushurl;
$disable_dsa = 1 unless $pushurl;
$disable_dsa = $disable_dsa ? 1 : '';
our $httpdnsname = ($gitweburl =~ m,https?://([A-Za-z0-9.-]+),i) ? lc($1) : undef if $gitweburl;
our $httpsdnsname = ($httpspushurl =~ m,https://([A-Za-z0-9.-]+),i) ? lc($1) : undef if $httpspushurl;
$SmartHTTPOnly = $SmartHTTPOnly ? 1 : '';
$TLSHost = $TLSHost ? 1 : '';
$pretrustedroot = $pretrustedroot ? 1 : '';
$suppress_git_ssh_logging = $suppress_git_ssh_logging ? 1 : '';
$git_daemon_any_host = $git_daemon_any_host ? 1 : '';
if ((!defined($git_daemon_host_list) || $git_daemon_host_list =~ /^\s*$/) &&
    (defined($gitpullurl) && $gitpullurl =~ m{^git://\[?[A-Za-z0-9.-:]}i)) {
	if ($gitpullurl =~ m{^[gG][iI][tT]://([A-Za-z0-9.-]+)(?:[/:]|$)} ||
	    $gitpullurl =~ m{^[gG][iI][tT]://\[([0-9a-zA-Z.:%]+)\](?:[/:]|$)}) {
		my $gdhn = lc($1);
		$gdhn ne "." and $gdhn =~ s/\.$//;
		my $gdhnl = $gdhn; $gdhnl =~ s/(?<!^)(?<!\.)\..*$//;
		$git_daemon_host_list="$gdhn";
		do {$git_daemon_host_list.=" $_" unless index(" $git_daemon_host_list "," $_ ")>=0}
			foreach $gdhnl, qw"localhost ::1 127.0.0.1";
	}
}
if (defined($git_daemon_host_list)) {
	$git_daemon_host_list = lc($git_daemon_host_list);
	$git_daemon_host_list =~ s/^\s+//;
	$git_daemon_host_list =~ s/\s+$//;
	$git_daemon_host_list = undef if $git_daemon_host_list eq "";
}
$mirror || $push or
	die "Girocco::Config: neither \$mirror nor \$push is set?!";
!$push || ($pushurl || $httpspushurl || $gitpullurl || $httppullurl) or
	die "Girocco::Config: no pull URL is set";
!$push || ($pushurl || $httpspushurl) or
	die "Girocco::Config: \$push set but \$pushurl and \$httpspushurl are undef";
!$mirror || $mirror_user or
	die "Girocco::Config: \$mirror set but \$mirror_user is undef";
$TLSHost = $TLSHost ? 1 : '';
$manage_users = $manage_users ? 1 : 0;
$chrooted = $chrooted ? 1 : 0;
$manage_users == $chrooted or
	die "Girocco::Config: \$manage_users and \$chrooted must be set to the same value";
!$chrooted || uc($permission_control) ne 'ACL' or
	die "Girocco::Config: resolving uids for ACL not supported when using chroot";
defined($permission_control) or $permission_control = '';
$permission_control = ucfirst(lc($permission_control));
(grep { $permission_control eq $_ } qw(Group Hooks)) or
	die "Girocco::Config: \$permission_control must be set to Group or Hooks";
$chrooted || !$mob or
	die "Girocco::Config: mob user supported only in the chrooted mode";
!$httpspushurl || $httpsdnsname or
	die "Girocco::Config: \$httpspushurl invalid does not start with https://domainname";
!$svn_log_window_size || $svn_log_window_size =~ /^[1-9][0-9]*$/ or
	die "Girocco::Config: \$svn_log_window_size must be undef or numeric";
defined($max_file_size512) && !$max_file_size512 and $max_file_size512 = undef;
!defined($max_file_size512) || $max_file_size512 =~ /^[1-9][0-9]*$/ && $max_file_size512 <= 2147483647 or
	die "Girocco::Config: \$max_file_size512 must be undef or a positive integer <= 2147483647";
defined($max_clone_objects) && !$max_clone_objects and $max_clone_objects = undef;
!defined($max_clone_objects) || $max_clone_objects =~ /^[1-9][0-9]*$/ or
	die "Girocco::Config: \$max_clone_objects must be undef or a positive integer";
!defined($posix_sh_bin) || $posix_sh_bin !~ /\s/ or
	die "Girocco::Config: \$posix_sh_bin must not contain any whitespace";
!defined($perl_bin) || $perl_bin !~ /\s/ or
	die "Girocco::Config: \$perl_bin must not contain any whitespace";
!$delay_gfi_redelta and $delay_gfi_redelta = undef;
!$git_no_mmap and $git_no_mmap = undef;
!$suppress_x_girocco and $suppress_x_girocco = undef;
!$jgit_compatible_bitmaps and $jgit_compatible_bitmaps = undef;
!$autogchack and $autogchack = undef;
!$reflogs_lifetime || $reflogs_lifetime !~ /^[1-9][0-9]*$/ and $reflogs_lifetime = 1;
$reflogs_lifetime = 0 + $reflogs_lifetime;
$reflogs_lifetime >= 0 or $reflogs_lifetime = 1;
$reflogs_lifetime <= 30 or $reflogs_lifetime = 30;
!defined $upload_pack_window || $upload_pack_window =~ /^[1-9][0-9]*$/ or
	die "Girocco::Config: \$upload_pack_window must be undef or numeric";
!defined $upload_pack_window || (2 <= $upload_pack_window && $upload_pack_window <= 50) or
	die "Girocco::Config: \$upload_pack_window must be in range 2..50";
!defined $max_receive_size || $max_receive_size =~ /^\d+[kKmMgG]?$/ or
	die "Girocco::Config: \$max_receive_size setting is invalid";
defined $git_shared_repository_setting or $git_shared_repository_setting = 2;
$git_shared_repository_setting = lc($git_shared_repository_setting);
{
	$git_shared_repository_setting =~ /^(?:false|umask|00*)$/ and do {
		$git_shared_repository_setting = 0;
		$var_umask = '0177';
		$var_umask_ug = $var_umask;
		last;
	};
	$git_shared_repository_setting =~ /^(?:true|group|1|00*1)$/ and do {
		$git_shared_repository_setting = 1;
		$var_umask = '0117';
		$var_umask_ug = $var_umask;
		last;
	};
	$git_shared_repository_setting =~ /^(?:all|world|everybody|2|00*2)$/ and do {
		$git_shared_repository_setting = 2;
		$var_umask = '0113';
		$var_umask_ug = '0117';
		last;
	};
	$git_shared_repository_setting =~ /^0[0-7]{1,3}$/ and do {
		my $p = oct($git_shared_repository_setting);
		$p &= ~0111;
		$p |=  0600;
		$git_shared_repository_setting = sprintf("0%03o", $p);
		$var_umask = sprintf("0%03o", $p ^ 0777);
		$var_umask_ug = sprintf("0%03o", ($p ^ 0777) | 007);
		last;
	};
	die "Invalid \$Girocco::Config::git_shared_repository_setting setting ($git_shared_repository_setting)\n";
}
$var_umask_perm = $permission_control eq 'Group' ? $var_umask : '0';
defined($ENV{'SENDMAIL_PL_HOST'}) and eval 'our $sendmail_pl_host = $ENV{"SENDMAIL_PL_HOST"}';
defined($ENV{'SENDMAIL_PL_PORT'}) and eval 'our $sendmail_pl_port = $ENV{"SENDMAIL_PL_PORT"}';
defined($ENV{'SENDMAIL_PL_NCBIN'}) and eval 'our $sendmail_pl_ncbin = $ENV{"SENDMAIL_PL_NCBIN"}';
defined($ENV{'SENDMAIL_PL_NCOPT'}) and eval 'our $sendmail_pl_ncopt = $ENV{"SENDMAIL_PL_NCOPT"}';
defined($ENV{'PYTHON'}) and eval 'our $python = $ENV{"PYTHON"}';

# jailreporoot MUST NOT be absolute
defined($jailreporoot) && substr($jailreporoot, 0, 1) ne "/" or
	die "Girocco::Config: \$jailreporoot MUST NOT be an absolute path\n";

# webreporoot can be undef
!defined($webreporoot) || substr($webreporoot, 0, 1) eq "/" or
	die "Girocco::Config: \$webreporoot MUST be an absolute path if not undef\n";

# All these MUST be absolute paths
{
	no strict 'refs';
	defined(${$_}) && substr(${$_}, 0, 1) eq "/" or
		die "Girocco::Config: \$$_ MUST be an absolute path\n"
	foreach qw(basedir certsdir reporoot chroot webroot cgiroot projlist_cache_dir);
}

# Make sure Git has a consistent and reproducible environment

$ENV{'XDG_CONFIG_HOME'} = $chroot.'/var/empty';
$ENV{'HOME'} = $chroot.'/etc/girocco';
$ENV{'TMPDIR'} = '/tmp';
$ENV{'GIT_CONFIG_NOSYSTEM'} = 1;
$ENV{'GIT_ATTR_NOSYSTEM'} = 1;
$ENV{'GIT_NO_REPLACE_OBJECTS'} = 1;
$ENV{'GIT_TERMINAL_PROMPT'} = 0;
$ENV{'GIT_ASKPASS'} = $basedir.'/bin/git-askpass-password';
delete $ENV{'GIT_USER_AGENT'};
$ENV{'GIT_USER_AGENT'} = $git_client_ua if defined($git_client_ua);
delete $ENV{'GIT_HTTP_USER_AGENT'};
delete $ENV{'GIT_CONFIG_PARAMETERS'};
delete $ENV{'GIT_ALTERNATE_OBJECT_DIRECTORIES'};
delete $ENV{'GIT_CONFIG'};
delete $ENV{'GIT_DIR'};
delete $ENV{'GIT_GRAFT_FILE'};
delete $ENV{'GIT_INDEX_FILE'};
delete $ENV{'GIT_OBJECT_DIRECTORY'};
delete $ENV{'GIT_NAMESPACE'};

# Guarantee a sane umask for Girocco

umask(umask() & ~0770);

1;
