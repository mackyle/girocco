package Girocco::Notify;

use strict;
use warnings;

use Girocco::Config;
use Girocco::Util;
use Girocco::HashUtil qw(hmac_sha1);
my $have_hmac_sha256;
BEGIN {eval{
	$have_hmac_sha256 = 0;
	require Digest::SHA;
	Digest::SHA->import( qw(hmac_sha256) );
	$have_hmac_sha256 = 1;
}}

use LWP::UserAgent;

#use RPC::XML;
#use RPC::XML::Client;

# This Perl code creates json payload within post-receive hook.

sub _fixlaxiso {
	# The %ai %ci formats are ISO 8601 like "1999-12-31 23:59:59 -0100"
	# The %aI %cI strict ISO 8601 formats aren't available until Git v2.2.0
	local $_ = shift;
	s/ /T/;			# "1999-12-31 23:59:59 -0100" -> "1999-12-31T23:59:59 -0100"
	s/ //;			# "1999-12-31T23:59:59 -0100" -> "1999-12-31T23:59:59-0100"
	s/(?<!:)(\d\d)$/:$1/;	# "1999-12-31T23:59:59-0100"  -> "1999-12-31T23:59:59-01:00"
	return $_;
}

sub json_commit {
	my ($proj, $commit, $root) = @_;

	my @gcmd = ($Girocco::Config::git_bin, '--git-dir='.$proj->{path});
	my $fd;

	open $fd, '-|', @gcmd, 'log', '-1', '--pretty=format:%T%n%ae %an%n%ai%n%ce %cn%n%ci%n%s%n%n%b', $commit, '--'
		or die "cannot do git log: $! $?";
	my @l = <$fd>;
	chomp @l;
	close $fd;
	my ($tr) = $l[0];
	my ($ae, $an) = ($l[1] =~ /^(.*?) (.*)$/);
	#my ($ai) = _fixlaxiso($l[2]);
	my ($ce, $cn) = ($l[3] =~ /^(.*?) (.*)$/);
	my ($ci) = _fixlaxiso($l[4]);
	my $msg = join("\n", splice(@l, 5));
	# Up to three trailing newlines in case of no body.
	chomp $msg; chomp $msg; chomp $msg;

	my ($rf, $af, $mf) = ([], [], []);
	my $parent = ($commit eq $root) ? "--root" : "$commit^";
	open $fd, '-|', @gcmd, 'diff-tree', '--name-status', '-r', $parent, $commit, '--'
		or die "cannot do git diff-tree: $! $?";
	while (<$fd>) {
		chomp;
		my ($s, $file) = split(/\t/, $_);
		if ($s eq 'M') {
			push @$mf, $file;
		} elsif ($s eq 'A') {
			push @$af, $file;
		} elsif ($s eq 'R') {
			push @$rf, $file;
		}
	}
	close $fd;

	return {
		"removed"   => $rf,
		"message"   => $msg,
		"added"     => $af,
		"timestamp" => $ci,
		"modified"  => $mf,
		"url"       => $Girocco::Config::gitweburl."/".$proj->{name}.".git/commit/".$commit,
		"author"    => { "name" => $an, "email" => $ae },
		"committer" => { "name" => $cn, "email" => $ce },
		"distinct"  => json_bool(1),
		"id"        => $commit,
		"tree_id"   => $tr
	}; 
}

sub _jsonagent {
	my $proj = shift;
	return "girocco/1.0 (JSON Push Notification) (" .
		$Girocco::Config::gitweburl."/".$proj->{name}.".git" .
		")";
}

sub _projurl {
	my ($base, $suffix) = @_;
	defined($base) && $base ne "" or return undef;
	defined($suffix) && $suffix ne "" or return undef;
	return $base.'/'.$suffix.'.git';
}

sub _jsonrepo {
	my $proj = shift;
	return {
		"name"  => $proj->{name},
		"default_branch" => $proj->{HEAD},
		"master_branch" => $proj->{HEAD},
		"description" => $proj->{desc},
		# Girocco extension: full_name is full project name,
		# equivalent to GitHub's "owner[name]/name".
		"full_name" => $proj->{name}.".git",

		"url" => _projurl($Girocco::Config::gitweburl,$proj->{name}),
		"html_url" => _projurl($Girocco::Config::gitweburl,$proj->{name}),
		"clone_url" => _projurl($Girocco::Config::httppullurl,$proj->{name}),
		"git_url" => _projurl($Girocco::Config::gitpullurl,$proj->{name}),
		"ssh_url" => _projurl($Girocco::Config::pushurl,$proj->{name}),
		"mirror_url" => $proj->{url},
		# Girocco extension: Git Pull URL.
		"pull_url" => _projurl($Girocco::Config::gitpullurl,$proj->{name}),

		"owner" => { "email" => $proj->{email} }
	};
}

sub _jsonpayload {
	my ($proj, $payload) = @_;
	my $ct = $proj->{jsontype};
	if (!defined($ct) || $ct ne 'application/json') {
		{
			use bytes;
			# Spaces are expected to be encoded as '+' rather than %20
			# This is not part of any RFC, but matches the behavior
			# of the specification that we're emulating here
			$payload =~ s/([^ A-Za-z0-9_.~-])/sprintf("%%%02X",ord($1))/ge;
			$payload =~ s/[ ]/+/g; # expected but NOT part of any RFC!
		}
		$payload = 'payload=' . $payload;
	}
	return $payload;
}

sub _jsonsigheaders {
	my ($proj, $payload) = @_;
	my $hmackey = $proj->{jsonsecret};
	my @sigheaders = ();
	if (defined($hmackey) && $hmackey ne "") {
		my $sig = "sha1=".lc(unpack('H*',hmac_sha1($payload, $hmackey)));
		push(@sigheaders, X_Hub_Signature => $sig);
		if ($have_hmac_sha256) {
			my $sig256 = "sha256=".lc(unpack('H*',hmac_sha256($payload, $hmackey)));
			push(@sigheaders, X_Hub_Signature_256 => $sig256);
		}
	}
	return @sigheaders;
}

sub json {
	my ($url, $proj, $user, $ref, $oldrev, $newrev, $forced) = @_;

	my $pusher = {};
	my $sender = {};
	if ($user) {
		$pusher = { "name" => $user->{name} };
		$sender = { "login" => $user->{name} };
		if ($user->{name} ne 'mob') {
			$pusher->{"email"} = $user->{email};
		}
	}

	my $commits = [];

	my @commits = get_commits($proj, $ref, $oldrev, $newrev);
	my $root = ($oldrev =~ /^0+$/) ? $commits[$#commits] : "";
	foreach my $commit (@commits) {
		push @$commits, json_commit($proj, $commit, $root);
	}

	# This is backwards-compatible with GitHub (except the GitHub-specific
	# full project name construction sometimes performed in clients)
	my $payload = to_json {
		"before"  => $oldrev,
		"after"   => $newrev,
		"ref"     => $ref,
		"created" => json_bool($oldrev =~ /^0+$/ ? 1 : 0),
		"deleted" => json_bool($newrev =~ /^0+$/ ? 1 : 0),
		"forced"  => json_bool($forced ? 1 : 0),
		"repository" => _jsonrepo($proj),
		"pusher" => $pusher,
		"sender" => $sender,
		"commits" => $commits
	};

	# print "$payload\n";
	my $ua = LWP::UserAgent->new(agent => _jsonagent($proj));
	$ua->max_redirect(0); # no redirects allowed
	$ua->max_size(32768); # there really shouldn't be hardly any response content at all
	$ua->timeout(5);
	my $ct = $proj->{jsontype};
	defined($ct) && $ct eq 'application/json' or
		$ct = 'application/x-www-form-urlencoded';
	$payload = _jsonpayload($proj, $payload);
	my @headers = ( Content_Type => $ct, Content_Length => length($payload) );
	$ua->post($url, @headers, _jsonsigheaders($proj, $payload), Content => $payload);
}


sub json_test_post {
	my ($proj, $url) = @_;
	defined($url) && $url ne "" or $url = $proj->{notifyjson};
	defined($url) && $url ne "" or return undef;
	my $ct = $proj->{jsontype};
	defined($ct) && $ct eq 'application/json' or
		$ct = 'application/x-www-form-urlencoded';
	my $hook = {
		"active" => json_bool(1),
		"config" => {
				"content_type" => (($ct =~ /json/) ? "json" : "form"),
				"url" => $url
			    },
		"events" => [ "push" ]
	};
	my $payload = to_json {
		"zen" => 'Why is a raven like a writing desk?',
		"repository" => _jsonrepo($proj),
		"hook" => $hook,
		"sender" => {
				"url" => _projurl($Girocco::Config::gitweburl,$proj->{name}),
				"html_url" => _projurl($Girocco::Config::gitweburl,$proj->{name}),
				"email" => $proj->{email}
			    }
	};
	$payload = _jsonpayload($proj, $payload);
	my $ua = LWP::UserAgent->new(agent => _jsonagent($proj));
	$ua->max_redirect(0); # no redirects allowed
	$ua->max_size(32768); # there really shouldn't be hardly any response content at all
	$ua->timeout(15); # Allow extra time on initial check
	my @headers = ( Content_Type => $ct, Content_Length => length($payload) );
	my $r = $ua->post($url, @headers, _jsonsigheaders($proj, $payload), Content => $payload);
	return $r->protocol !~ m{HTTP/0\.9}i && $r->is_success; # HTTP/0.9 is NOT okay for this test
}


sub cia_commit {
	my ($cianame, $proj, $branch, $commit, $root) = @_;

	my @gcmd = ($Girocco::Config::git_bin, '--git-dir='.$proj->{path});
	my $fd;

	open $fd, '-|', @gcmd, 'log', '-1', '--pretty=format:%an <%ae>%n%at%n%s', $commit, '--'
		or die "cannot do git log: $! $?";
	my @l = <$fd>;
	chomp @l;
	close $fd;
	foreach (@l) { s/&/&amp;/g; s/</&lt;/g; s/>/&gt;/g; }
	my ($a, $at, $subj) = @l;

	my @f;
	my $parent = ($commit eq $root) ? "--root" : "$commit^";
	open $fd, '-|', @gcmd, 'diff-tree', '--name-status', '-r', $parent, $commit, '--'
		or die "cannot do git diff-tree: $! $?";
	while (<$fd>) {
		chomp;
		s/&/&amp;/g; s/</&lt;/g; s/>/&gt;/g;
		my ($status, $file) = split(/\t/, $_);
		push @f, $file;
	}
	close $fd;

	my $rev = substr($commit, 0, 12);

	my $msg = <<EOT;
	<message>
		<generator>
			<name>Girocco::Notify</name>
			<version>1.0</version>
		</generator>
		<source>
			<project>$cianame</project>
EOT
	if ($branch ne 'master') { # XXX: Check HEAD instead
		$msg .= "<branch>$branch</branch>";
	}
	$msg .= "</source>\n";
	$msg .= "<timestamp>$at</timestamp>\n";
	$msg .= "<body><commit><author>$a</author><revision>$rev</revision>\n";
	$msg .= "<url>$Girocco::Config::gitweburl/$proj->{name}.git/commit/$commit</url>\n";
	$msg .= "<files>\n";
	foreach (@f) { $msg .= "<file>$_</file>\n"; }
	$msg .= "</files><log>$subj</log></commit></body></message>\n";

	# print "$msg\n";
	#my $rpc_client = new RPC::XML::Client "http://cia.vc/RPC2";
	#my $rpc_request = RPC::XML::request->new('hub.deliver', $msg);
	#my $rpc_response = $rpc_client->send_request($rpc_request);
	#ref $rpc_response or print STDERR "XML-RPC Error: $RPC::XML::ERROR\n";
}

sub cia {
	my ($cianame, $proj, $ref, $oldrev, $newrev) = @_;

	# CIA notifications for branches only
	my $branch = $ref;
	$branch =~ s#^refs/heads/## or return;

	my @commits = get_commits($proj, $ref, $oldrev, $newrev);
	my $root = ($oldrev =~ /^0+$/) ? $commits[$#commits] : "";
	foreach my $commit (@commits) {
		cia_commit($cianame, $proj, $branch, $commit, $root);
	}
	print "$proj->{name}.git: CIA.vc is defunct ($cianame)\n";
}


sub get_commits {
	my ($proj, $ref, $oldrev, $newrev) = @_;

	return () if $newrev =~ /^0+$/;

	my @gcmd = ($Girocco::Config::git_bin, '--git-dir='.$proj->{path});
	my $fd;

	open $fd, '-|', @gcmd, 'for-each-ref', '--format=%(refname)', 'refs/heads/'
		or die "cannot do git for-each-ref: $! $?";
	my @refs = <$fd>;
	chomp @refs;
	@refs = grep { $_ ne $ref } @refs;
	close $fd;

	my @revlims;
	if (@refs) {
		open $fd, '-|', @gcmd, 'rev-parse', '--not', @refs
			or die "cannot do git rev-list for revlims: $! $?";
		@revlims = <$fd>;
		chomp @revlims;
		close $fd;
	}

	my $revspec = (($oldrev =~ /^0+$/) ? $newrev : "$oldrev..$newrev");
	open $fd, '-|', @gcmd, 'rev-list', @revlims, $revspec
		or die "cannot do git rev-list: $! $?";
	my @revs = <$fd>;
	chomp @revs;
	close $fd;

	return @revs;
}

sub _get_sender_uuid {
	my ($proj, $user) = @_;

	my $sender;
	my $xtrahdr = '';
	my $senderemail = $proj->{email};
	defined($senderemail) && $senderemail ne ''
		or $senderemail = $Girocco::Config::sender;
	if ($user) {
		if ($user->{name} eq 'mob') {
			$sender = "The Mob User <$senderemail>";
		} else {
			my $useremail = $user->{email};
			defined($useremail) && $useremail ne ''
				or $useremail = $senderemail;
			$sender = "$user->{name} <$useremail>";
			$xtrahdr = "X-User-UUID: $user->{uuid}" if $user->{uuid};
		}
	} else {
		$sender = "$proj->{name} <$senderemail>";
	}

	return ($sender, $xtrahdr);
}

sub _notify_for_ref {
	$_[0] =~ m{^refs/heads/.} || $_[0] =~ m{^refs/tags/.};
}

my $_ref_change;
BEGIN {$_ref_change = sub {
	my ($proj, $user, $ref, $oldrev, $newrev) = @_;
	_notify_for_ref($ref) or return (0, 0, undef);

	my $mail_sh_ran = 0;
	my $git_ran = 0;
	my $ind = undef;
	chdir($proj->{path});

	# First, possibly send out various mails
	if (($ref =~ m{^refs/heads/.} && $proj->{notifymail}) ||
	    ($ref =~ m{^refs/tags/.} && ($proj->{notifymail} || $proj->{notifytag}))) {
		my ($sender, $xtrahdr) = _get_sender_uuid($proj, $user);
		my @cmd = ($Girocco::Config::basedir.'/taskd/mail.sh',
			"$ref", "$oldrev", "$newrev", $proj->{name},
			$sender, $xtrahdr);
		if (system(@cmd)) {
			if ($? < 0 && exists($ENV{MAIL_SH_OTHER_BRANCHES})) {
				# Let's go again
				delete $ENV{MAIL_SH_OTHER_BRANCHES};
				system(@cmd);
			}
			$? and warn "mail.sh failed";
		}
		$mail_sh_ran = 1;
	}

	# Next, send JSON packet to given URL if enabled.
	if ($proj->{notifyjson}) {
		($ind, $git_ran) = ref_indicator($proj->{path}, $oldrev, $newrev);
		json($proj->{notifyjson}, $proj, $user, $ref, $oldrev, $newrev, $ind eq '...');
	}

	# Also send CIA notifications.
	if ($proj->{notifycia}) {
		cia($proj->{notifycia}, $proj, $ref, $oldrev, $newrev);
	}

	return ($mail_sh_ran, $git_ran, $ind);
}}

# ref_changes($proj, $user, [$coderef,] [$oldrefs,] @changes)
# $coderef gets called with ($oldrev, $newrev, $refname, $mail_sh_ran, $git_ran, $ind)
# as each update is processed where $mail_sh_ran will be true if mail.sh was run
# $ran_git will be true if git was run and $ind will be undef unless ref_indicator
# was run in which case it will be the ref_indicator '..' or '...' result
# $oldrefs is an optional hash of $$oldrefs{$refname} = $oldvalue
# @changes is array of [$oldrev, $newrev, $ref]
sub ref_changes {
	use IPC::Open2;

	my $proj = shift;
	my $user = shift;
	my $proc;
	my $oldrefs;
	$proc = shift if ref($_[0]) eq 'CODE';
	$oldrefs = shift if ref($_[0]) eq 'HASH';
	return if !@_ || ref($_[0]) ne 'ARRAY' || @{$_[0]} != 3 ||
		!${$_[0]}[0] || !${$_[0]}[1] || !${$_[0]}[2];

	# run custom notify if present
	# hook protocol is the same as for Git's post-receive hook where each
	# line sent to the hook's stdin has the form:
	#     oldhash newhash fullrefname
	# with the following additions:
	#   * four command line arguments are passed:
	#      1. project name (e.g. "proj" "proj/fork" "proj/fork/sub" etc.)
	#      2. "user" responsible for the changes
	#      3. total number of lines coming on stdin
	#      4. how many of those lines are "context" lines (sent first)
	#   * "context" lines are sent first before any actual change lines
	#   * "context" lines have the same format except oldhash equals newhash
	#   * "context" lines give the value unchanged refs had at the time
	#     the changes to the non-"context" refs were made
	#   * currently "context" lines are only provided for "refs/heads/..."
	#   * current directory will always be the repository's top-level $GIT_DIR
	#   * the PATH is guaranteed to find the correct Git, utils and $basedir/bin
	#   * the hook need not consume all (or any) of stdin
	#   * the exit code for the hook is ignored (just like Git's post-receive)
	#   * the GIROCCO_BASEDIR environment variable is set to $Girocco::Config::basedir
	my $customhook;
	if (($customhook = $proj->_has_notifyhook)) {{
		my @argv = ();
		my $argv0;
		if (is_shellish($customhook)) {
			$argv0 = $Girocco::Config::posix_sh_bin || "/bin/sh";
			push(@argv, $argv0, "-c", $customhook.' "$@"');
		} else {
			-f $customhook && -x _ or last;
			$argv0 = $customhook;
		}
		my $username = "";
		$username = $user->{name} if $user && defined($user->{name});
		$username ne "" or $username = "-";
		push(@argv, $argv0, $proj->{name}, $username);
		my @mod = grep({$$_[2] =~ m{^refs/.} && $$_[1] ne "" && $$_[2] ne "" && $$_[1] ne $$_[2]} @_);
		my %modref = map({($$_[2] => 1)} @mod);
		my %same = ();
		do {do {$same{$_} = $$oldrefs{$_} if !exists($modref{$_})} foreach keys %$oldrefs} if $oldrefs;
		my @same = ();
		push(@same, [$same{$_}, $same{$_}, $_]) foreach sort keys %same;
		push(@argv, @same + @mod, @same + 0);
		chdir($proj->{path}) or last;
		local $ENV{GIROCCO_BASEDIR} = $Girocco::Config::basedir;
		local $ENV{PATH} = util_path;
		if (open my $hookpipe, '|-', @argv) {
			local $SIG{'PIPE'} = sub {};
			print $hookpipe map("$$_[0] $$_[1] $$_[2]\n", @same, @mod);
			close $hookpipe;
		} else {
			print STDERR "$proj->{name}: failed to run notifyhook \"$customhook\": $!\n";
		}
	}}

	# don't even try the fancy stuff if there are too many heads
	my $maxheads = int(100000 / (1 + length(${$_[0]}[0])));
	my $newheadsdone = 0;
	my @newheads = grep({$$_[2] =~ m{^refs/heads/.} && $$_[1] !~ /^0+$/} @_);

	if ($oldrefs && @newheads && keys(%$oldrefs) + @newheads <= $maxheads) {{
		my $projarg = '--git-dir='.$proj->{path};

		# git merge-base --independent requires v1.7.3 or later
		# We run it and if it fails just end up not processing indep heads last
		# There is no version of merge-base --independent that accepts stdin revs
		my %indep = map { $_ => 1 } split(' ',
			get_git($projarg, 'merge-base', '--independent', map($$_[1], @newheads)) || '');

		# We pass the revisions on stdin so this should never fail unless one of
		# the input revisions is no longer valid (which should never happen).
		# However, if it does, we just fall back to the old non-fancy technique.
		my ($inp, $out, $pid2);
		$pid2 = eval { open2($out, $inp, $Girocco::Config::git_bin, $projarg,
				'rev-list', '--no-walk', '--reverse', '--stdin') };
		$pid2 or last;
		{
			local $SIG{'PIPE'} = sub {};
			print $inp map("$$_[1]\n", @newheads);
			close $inp;
		}
		my @ordered = <$out>;
		close $out;
		waitpid $pid2, 0;
		@ordered or last; # if we got nothing it failed
		chomp(@ordered);

		my %updates = ();
		push(@{$updates{$$_[1]}}, $_) foreach @newheads;
		my %curheads = %$oldrefs;
		my $headcmp = sub {
			if ($_[0] eq 'refs/heads/master') {
				return ($_[1] eq 'refs/heads/master') ? 0 : -1;
			} elsif ($_[1] eq 'refs/heads/master') {
				return 1;
			} else {
				return $_[0] cmp $_[1];
			}
		};
		foreach (grep(!$indep{$_}, @ordered), grep($indep{$_}, @ordered)) {
			foreach my $change (sort {&$headcmp($$a[2], $$b[2])} @{$updates{$_}}) {
				my ($old, $new, $ref) = @$change;
				$ENV{MAIL_SH_OTHER_BRANCHES} = join(' ', values(%curheads));
				my ($mail_sh_ran, $git_ran, $ind) =
					&$_ref_change($proj, $user, $ref, $old, $new);
				&$proc($old, $new, $ref, $mail_sh_ran, $git_ran, $ind) if $proc;
				$curheads{$ref} = $new;
			}
		}
		$newheadsdone = 1;
	}}

	delete $ENV{MAIL_SH_OTHER_BRANCHES};
	foreach (@_) {
		my ($old, $new, $ref) = @$_;
		if (!$newheadsdone || $ref !~ m{^refs/heads/.} || $new =~ /^0+$/) {
			my $ran_mail_sh = &$_ref_change($proj, $user, $ref, $old, $new);
			&$proc($old, $new, $ref, $ran_mail_sh) if $proc;
		}
	}
}

1;
