#!/bin/sh
#
# jobd - Perform Girocco maintenance jobs
# The old jobd.sh has been replaced by jobd.pl; it now simply runs that.

exec @basedir@/jobd/jobd.pl "$@"
