#!/bin/sh

# Girocco installs a pre-auto-gc hook.
# When $GIROCCO_DIVERT_GIT_SVN_AUTO_GC is set to 1
# that hook, when executed, runs this script and
# no matter what exit code this script uses, the
# pre-auto-gc hook always exits with a code of 1
# immediately after this script finishes thereby
# aborting any Git-directed gc.

# The `git-svn fetch` operation periodically runs
# `git gc --auto` which ultimately causes this
# script to run provided the correct conditions
# are met and $GIROCCO_DIVERT_GIT_SVN_AUTO_GC is
# set to 1 in the environment.

# Both the jobd/update.sh and taskd/clone.sh scripts
# set and export GIROCCO_DIVERT_GIT_SVN_AUTO_GC=1
# and force `gc.auto=1` as well whenever they process
# a `git-svn` mirror.

. @basedir@/shlib.sh
. @basedir@/jobd/gc-util-functions.sh

set -e

[ -d objects ] || exit 1

umask 002
[ "$cfg_permission_control" != "Hooks" ] || umask 000

v_get_proj_from_dir proj
proj="${proj%.git}"

if lotsa_loose_objects_or_sopacks; then
	echo "Packing loose objects ($(date))"
	if pack_incremental_loose_objects_if_lockable; then
		echo "Packing and pruning complete ($(date))"
	else
		echo "Packing skipped: $lockerr ($(date))"
	fi
fi

exit 0
