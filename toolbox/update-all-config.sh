#!/bin/sh
#
# update-all-config - Update all out-of-date config
# The old update-all-config.sh has been replaced by update-all-config.pl;
# it now simply runs that.

exec @basedir@/toolbox/update-all-config.pl "$@"
