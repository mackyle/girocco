#!/bin/sh

# Report on project repository status.
# Output can be sent as email to admin with -m
# Automatically runs with nice and ionice (if available)

set -e

datefmt='%Y-%m-%d %H:%M:%S %z'
startdate="$(date "+$datefmt")"

. @basedir@/shlib.sh
PATH="$cfg_basedir/bin:${PATH:-$(/usr/bin/getconf PATH)}"
export PATH

USAGE="${0##*/} [-hmEW] [--full] [<project>...]"
HELP="
NAME
       ${0##*/} - report on \"health\" of Girocco repositories

SYNOPSIS
       $USAGE

DESCRIPTION
       The ${0##*/} script checks all Girocco repositories for
       consistency and includes a count of empty repositories as well.

       The results can automatically be mailed to the Girocco admin or
       simply displayed on STDOUT or optionally suppressed if there are
       no errors or warnings.

       Note that this utility currently provides absolutely no progress
       reporting even when run on a terminal so there will be no output
       whatsoever until all projects have been checked.

       The \"checking\" runs using nice and, if available, ionice.

       Projects not listed in \$chroot/etc/group will not be checked.

OPTIONS
       -h     show this help

       -m     e-mail results to the \$Girocco::Config::admin address.
              This option suppresses output to STDOUT.

       -E     Suppress output (i.e. no e-mail will be sent if -m has been used)
              unless at least one error has been detected.  If this flag is
              used WITHOUT -W and warnings occur, output will still be
              suppressed.

       -W     Suppress output unless at least one error or warning has been
              detected.  This flag overrides -E and will always produce output
              when only warnings are detected.

       --full Run a full \"git fsck\" check on each repository.  Without this
              option a much, much, much (5x or more) faster check is done on
              each repository using \"git rev-list --objects --all\" that will
              verify all objects reachable from any ref are present but does
              not perform all the validation checks nor does it verify the
              integrity of 'blob' objects (just that they are present).

              If the default Girocco 'transfer.fsckObjects=true' option has
              been left intact, use of the \"--full\" option should not
              normally be necessary.

       --no-full
              Disables --full.  Present for completeness.

       --reflog
              Check ref logs too.  Implied by \"--full\" but not by \"--no-full\".

       --indexed-objects
              Check indexed objects too.  Implied by \"--full\" but not by
              \"--no-full\".  Requires Git version 2.2.0 or later if not using
              the \"--full\" option to do it.

       --list Show a list of all projects checked in the order checked.
              This is the default if any specific projects are given.

       --no-list
              Do NOT show a list of each project name checked.
              This is the default when all projects are being checked.

       <project>
              Optionally one or more Girocco project names may be given to
              check only those projects.

TIPS
       Leave Girocco's default 'transfer.fsckObjects=true' setting alone and
       have a cron job run this utility in the default --no-full mode using the
       -m and -E options once a week to detect repository corruption.

       The default --no-full mode will detect any fatal errors with all
       reachable 'tag', 'commit' and 'tree' objects.  The --no-full mode will
       detect missing 'blob' objects but not corrupt 'blob' objects.

       The --no-full mode is an order of magnitude faster than --full mode and
       does not complain about non-fatal object errors.
"

usage() {
	if [ "${1:-1}" = "1" ]; then
		printf 'usage: %s\n' "$USAGE" >&2
		return 0
	fi
	printf '%s\n' "$HELP"
}

mailresult=
minoutput=0 # 0 => always output, 1 => if warn or err, 2 => if err
fullcheck=
onlylist=
shownames=
reflog=
indexedobj=

while [ $# -gt 0 ]; do
	if [ "${1#-[!-]?}" != "$1" ]; then
		_rest="${1#-?}"
		_next="${1%"$_rest"}"
		shift
		set -- "$_next" "-$_rest" "$@"
	fi
	case "$1" in
	"-?")
		usage 1 2>&1
		exit 0
		;;
	-h|--help)
		usage 2
		exit 0
		;;
	-m|--mail)
		mailresult=1
		;;
	-E)
		[ "${minoutput:-0}" != "0" ] || minoutput=2
		;;
	-W)
		minoutput=1
		;;
	--full)
		fullcheck=1
		;;
	--no-full)
		fullcheck=
		;;
	--reflog)
		reflog=1
		;;
	--indexed-objects)
		indexedobj=1
		;;
	--list)
		shownames=1
		;;
	--no-list)
		shownames=0
		;;
	--)
		shift
		break
		;;
	-?*)
		echo "${0##*/}: unknown option: $1" >&2
		usage 1
		exit 1
		;;
	*)
		break
		;;
	esac
	shift
done

[ -n "$shownames" ] || [ $# -eq 0 ] || shownames=1

while [ $# -gt 0 ]; do
	aproj="${1%.git}"
	shift
	if ! [ -d "$cfg_reporoot/$aproj.git" ]; then
		echo "${0##*/}: fatal: no such dir: $cfg_reporoot/$aproj.git" >&2
		exit 1
	fi
	if ! is_git_dir "$cfg_reporoot/$aproj.git"; then
		echo "${0##*/}: fatal: not a git dir: $cfg_reporoot/$aproj.git" >&2
		exit 1
	fi
	onlylist="${onlylist:+$onlylist }$aproj.git"
done

hasnice=
! command -v nice >/dev/null || hasnice=1
hasionice=
! command -v ionice >/dev/null || hasionice=1

nl='
'

projlist="$(cut -d : -f 1 <"$cfg_chroot/etc/group")"

is_listed_proj() {
	echo "$projlist" | grep -q -e "^$1$"
}

is_empty_proj() {
	# if packed-refs is empty and no files in refs then empty
	# we do NOT want to run any git command in case the repo is bad
	_pd="$cfg_reporoot/$1.git"
	if [ -f "$_pd/packed-refs" ] && [ -s "$_pd/packed-refs" ]; then
		if [ $(LC_ALL=C sed -n '/^#/!p' <"$_pd/packed-refs" | LC_ALL=C wc -l) -gt 0 ]; then
			return 1
		fi
	fi
	test $(find -L "$_pd/refs" -type f -print 2>/dev/null | head -n 1 | LC_ALL=C wc -l) -eq 0
}

get_check_proj() (
	cd "$cfg_reporoot/$1.git" || {
		echo "no such directory: $cfg_reporoot/$1.git"
		return 1
	}
	if [ -n "$fullcheck" ]; then
		# use git fsck
		# using --strict changes "zero-padded file modes" from a warning into an error
		# which we do NOT want so we do NOT use --strict
		cmd="git fsck"
		[ -z "$var_have_git_1710" ] || cmd="$cmd --no-dangling"
		# no need for --no-progress (v1.7.9+) since stderr will not be a tty
		cmd="$cmd 2>&1"
	else
		# use git rev-list --objects --all
		cmd="git rev-list --objects --all${reflog:+ --reflog}${indexedobj:+ --indexed-objects}"
		# but we only want stderr output not any of the objects list
		cmd="$cmd 2>&1 >/dev/null"
	fi
	[ -z "$hasionice" ] || cmd="ionice -c 3 $cmd"
	[ -z "$hasnice" ] || cmd="nice -n 19 $cmd"
	fsckresult=0
	fsckoutput="$(eval "$cmd")" || fsckresult=$?
	if [ -n "$fullcheck" ] && [ -z "$var_have_git_1710" ]; then
		# strip lines starting with "dangling" since --no-dangling is not supported
		# note that "dangling" is NOT translated
		fsckoutput="$(printf '%s\n' "$fsckoutput" | LC_ALL=C sed -n '/^dangling/!p')"
	fi
	[ -z "$fsckoutput" ] || printf '%s\n' "$fsckoutput"
	return $fsckresult
)

cd "$cfg_reporoot"
absroot="$(pwd -P)"
# howmany is how many fsck was run on plus how many were empty
howmany=0
# mtcount is how many were empty
mtcount=0
# okcount is how many fsck returned 0 status for
okcount=0
# warncount is how many fsck returned 0 status but non-empty output
warncount=0
# errresults are results from fsck non-0 status fsck runs
errresults=
# warnresults are non-empty results from 0 status fsck runs
warnresults=
# list of all projects checked in order if --list is active
allprojs=

while IFS='' read -r proj; do
	if [ -L "$proj" ]; then
		# symlinks to elsewhere under $cfg_reporoot are ignored
		# symlinks to outside there are reported on unless orphaned
		absproj="$(cd "$proj" && pwd -P)" && [ -n "$absproj" ] &&
			[ -d "$absproj" ] || continue
		case "$absproj" in "$absroot"/*)
			continue
		esac
	fi
	proj="${proj#./}"
	proj="${proj%.git}"
	is_listed_proj "$proj" && is_git_dir "$cfg_reporoot/$proj.git" || continue
	[ -d "$proj.git/objects" ] || continue
	[ "${shownames:-0}" = "0" ] || allprojs="${allprojs:+$allprojs }$proj"
	howmany="$(( $howmany + 1 ))"
	if is_empty_proj "$proj"; then
		mtcount="$(( $mtcount + 1 ))"
		continue
	fi
	ok=1
	output="$(get_check_proj "$proj")" || ok=
	[ -z "$ok" ] || okcount="$(( $okcount + 1 ))"
	[ -n "$ok" ] || [ -n "$output" ] || output="git fsck failed with no output"
	if [ -n "$output" ]; then
		output="$(printf '%s\n' "$output" | LC_ALL=C sed 's/^/    /')$nl"
		if [ -n "$ok" ]; then
			# warning
			warncount="$(( $warncount + 1 ))"
			[ -z "$warnresults" ] || warnresults="$warnresults$nl"
			warnresults="$warnresults$proj: (warnings only)$nl$output"
		else
			[ -z "$errresults" ] || errresults="$errresults$nl"
			errresults="$errresults$proj: (errors found)$nl$output"
		fi
	fi
done <<EOT
$(
	if [ -n "$onlylist" ]; then
		printf '%s\n' $onlylist
	else
		find -L . -type d \( -path ./_recyclebin -o -path ./_global -o -name '*.git' -print \) -prune 2>/dev/null |
		LC_ALL=C sort -f
	fi
)
EOT

enddate="$(date "+$datefmt")"
[ "$minoutput" != "1" ] || [ "$howmany" != "$(( $okcount + $mtcount ))" ] || [ "$warncount" != "0" ] || exit 0
[ "$minoutput" != "2" ] || [ "$howmany" != "$(( $okcount + $mtcount ))" ] || exit 0
domail=cat
[ -z "$mailresult" ] || domail='mailref "fsck@$cfg_gitweburl" -s "[$cfg_name] Project Fsck Status Report" "$cfg_admin"'
{
	cat <<EOT
Project Fsck Status Report
==========================

       Start Time: $startdate
         End Time: $enddate

 Projects Checked: $howmany
    Projects Okay: $(( $okcount + $mtcount )) (passed + warned + empty)

  Projects Passed: $(( $okcount - $warncount ))
  Projects Warned: $warncount
   Projects Empty: $mtcount
  Projects Failed: $(( $howmany - $mtcount - $okcount ))
EOT
	if [ "${shownames:-0}" != "0" ] && [ -n "$allprojs" ]; then
		prefix="\
 Projects Checked: "
		for aproj in $allprojs; do
			printf '%s%s\n' "$prefix" "$aproj"
			prefix="\
                   "
		done
	fi

	echo ""

	if [ -n "$errresults" ] || [ -n "$warnresults" ]; then
		echo ""
		echo "Fsck Output"
		echo "-----------"
		echo ""
		printf '%s' "$errresults"
		[ -z "$errresults" ] || [ -z "$warnresults" ] || echo ""
		printf '%s' "$warnresults"
		echo ""
	fi
} | eval "$domail"
