#!/usr/bin/env perl

# check-perl-modules.pl -- check for modules used by Girocco
# Copyright (c) 2013,2019,2020,2021 Kyle J. McKay.  All rights reserved.
# License GPLv2+: GNU GPL version 2 or later.
# www.gnu.org/licenses/gpl-2.0.html
# This is free software: you are free to change and redistribute it.
# There is NO WARRANTY, to the extent permitted by law.

# Attempt to require all the modules used by Perl anywhere in Girocco and
# report the failures.  There is special handling for the SHA-1 functions
# and some other optional modules.

# These ones are not errors

eval {require Proc::ProcessTable; 1} or
print "Note: missing Perl module Proc::ProcessTable, deprecated ".
	"toolbox/kill-stale-daemons.pl will not function\n";

eval {require HTML::Entities; 1} or
print "Warning: missing Perl module HTML::Entities, gitweb ".
	"email obfuscation will be partially disabled\n";

eval {require HTML::TagCloud; 1} or
print "Warning: missing Perl module HTML::TagCloud, optional gitweb tag cloud ".
	"display will be all one size\n";

eval {require HTTP::Date; 1} ||
eval {require Time::ParseDate; 1} or
print "Warning: missing Perl module HTTP::Date and Time::ParseDate, gitweb ".
	"will always return true to if-modified-since feed requests\n";

# These ones are required

my $errors = 0;

# For SHA1, any of Digest::SHA, Digest::SHA1 or Digest::SHA::PurePerl will do

eval {require Digest::SHA; 1} ||
eval {require Digest::SHA1; 1} ||
eval {require Digest::SHA::PurePerl; 1} or
++$errors,
print STDERR "One of the Perl modules Digest::SHA or Digest::SHA1 or "
. "Digest::SHA::PurePerl must be available\n";

my @modules = qw(
	CGI
	CGI::Carp
	CGI::Util
	Compress::Zlib
	Digest::MD5
	Encode
	Errno
	Fcntl
	File::Basename
	File::Find
	File::Spec
	File::stat
	Getopt::Long
	IO::Handle
	IO::Socket
	IO::Socket::UNIX
	IPC::Open2
	LWP::UserAgent
	MIME::Base64
	Pod::Usage
	POSIX
	Socket
	Storable
	Time::HiRes
	Time::Local
);

my %imports = (
	Time::HiRes => [qw(gettimeofday tv_interval)],
);

foreach my $module (@modules) {
	my $imports = '';
	$imports = " qw(".join(' ', @{$imports{$module}}).")"
		if $imports{$module};
	eval "use $module$imports; 1" or
	++$errors, print STDERR "Error: required Perl module $module$imports not found\n";
}

eval {require LWP::UserAgent; 1} or
print STDERR "Note: Perl module LWP::UserAgent ".
	"is required by the ref-change notification mechanism\n";

my $fcgi_ok = 1;
eval {require FCGI; 1} or $fcgi_ok = 0,
print STDERR "Note: missing Perl module FCGI used for efficient ".
	"gitweb.cgi FastCGI implementation\n";
my $cgifast_ok = 1;
eval {require CGI::Fast; 1} or $cgifast_ok = 0,
print STDERR "Note: missing Perl module CGI::Fast used for efficient ".
	"gitweb.cgi FastCGI implementation\n";
my $mods = "";
$mods .= "FCGI" unless $fcgi_ok;
$mods .= ($mods?" and ":"")."CGI::Fast" unless $cgifast_ok;
my $pl = "";
$pl = "s" unless $fcgi_ok || $cgifast_ok;
$mods eq "" or
print STDERR "Warning: gitweb.cgi running in FastCGI mode will use a pure Perl ".
	"implementation of FastCGI\n".
	"Warning: install Perl module$pl $mods for a more efficient gitweb.cgi ".
	"FastCGI implementation\n";
eval {require FCGI::ProcManager; 1} or
print STDERR "Note: gitweb.cgi running in FastCGI mode requires ".
	"missing FCGI::ProcManager to support the --nproc option\n";

!$errors or print STDERR "\nFatal: required perl modules are missing!\n\n";

exit($errors ? 1 : 0);
