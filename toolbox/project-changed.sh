#!/bin/sh

# A project has been changed outside Girocco, take suitable action.

# This script should be run for a project whenever:
#
# 1) It's been renamed (use the new name and run AFTER renaming)
# 2) The project's owner (or any other .git/config value) has been changed
# 3) Any of the project's refs have been changed (including the HEAD symref)
# 4) The project was manually changed in some other way and you're unsure
#
# Normally Girocco automatically takes care of everything this script does;
# so this script only ever need be run if an administrator manually makes
# changes to a project.

set -e

. @basedir@/shlib.sh

umask 002

p="${1%.git}"
if [ "$#" -ne 1 ] || [ -z "$p" ]; then
	echo "I need a project name (e.g. \"$(basename "$0") example\")"
	exit 1
fi
"$cfg_basedir/gitweb/genindex.sh" "$p" || :
if [ -d "$cfg_reporoot/$p.git" ]; then
	! [ -d "$cfg_reporoot/$p.git/htmlcache" ] ||
		>"$cfg_reporoot/$p.git/htmlcache/changed"
else
	echo "WARNING: no such directory: $cfg_reporoot/$p.git"
	echo "WARNING: but the project list file was updated anyway"
fi
