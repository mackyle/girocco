#!/usr/bin/perl

# Remove all files present in the htmlcache subdirectory of each project.
# A count of projects that had files to be removed is displayed.

use strict;
use warnings;

use lib "__BASEDIR__";
use Girocco::Config;
use Girocco::CLIUtil;
use Girocco::Project;

nice_me(18);
my $bd = $Girocco::Config::reporoot . '/';
my @projects = Girocco::Project::get_full_list();
my $progress = Girocco::CLIUtil::Progress->new(
	scalar(@projects), "Clearing htmlcache files");
my $cleared = 0;
foreach (@projects) {
	my $hcd = $bd . $_ . ".git/htmlcache";
	opendir my $dh, $hcd or next;
	my @files = map({/^(?![.][.]?$)(.*)$/ ? $1 : ()} readdir($dh));
	closedir $dh;
	unlink($hcd . "/" . $_) foreach @files;
	@files and ++$cleared;
} continue {$progress->update}
$progress = undef;
print "Projects cleared: $cleared\n";
exit 0
