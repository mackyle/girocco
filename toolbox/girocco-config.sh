#!/bin/sh

# Girocco "global" git config convenience tool
# Usage: girocco-config.sh <git config args>
# The purpose of this tool is simply to provide a convenient wrapper
# for adjusting the config values in the $chroot/etc/girocco/.gitconfig file.
# Directory permissions need to be manipulated before and after and it's
# clumsy to have to provide the longish --file=... argument.
# Hence this script.  it's semantically just an alias for running:
#   git config --file="$Girocco::Config::chroot/etc/girocco/.gitconfig" "$@"

set -e

. @basedir@/shlib.sh

trap 'chmod a-w "$cfg_chroot/etc/girocco"' EXIT
trap 'exit 129' HUP
trap 'exit 130' INT
trap 'exit 131' QUIT
trap 'exit 134' ABRT
trap 'exit 141' PIPE
trap 'exit 142' ALRM
trap 'exit 143' TERM

ec=
chmod u+w "$cfg_chroot/etc/girocco"
git config --file="$cfg_chroot/etc/girocco/.gitconfig" "$@" && ec=0 || ec=$?
chmod a-w "$cfg_chroot/etc/girocco"
trap - EXIT
exit ${ec:-1}
