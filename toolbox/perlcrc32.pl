#!/usr/bin/env perl

# perlcrc32.pl - display CRC-32 checksum value

# Copyright (C) 2020 Kyle J. McKay.
# All rights reserved.
#
# Permission to use, copy, modify, and distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

# Version 1.0.1

use strict;
use warnings;
use bytes;

use File::Basename qw(basename);
use Compress::Zlib qw(crc32);
use Getopt::Long;
use Pod::Usage;
BEGIN {
	eval 'require Pod::Text::Termcap; 1;' and
	@Pod::Usage::ISA = (qw( Pod::Text::Termcap ));
	defined($ENV{PERLDOC}) && $ENV{PERLDOC} ne "" or
	$ENV{PERLDOC} = "-oterm -oman";
}
my $me = basename($0);
close(DATA) if fileno(DATA);

exit(&main(@ARGV)||0);

my ($opt_c, $opt_d, $opt_v, $opt_x);

my $totalbytesread; BEGIN { $totalbytesread = 0 }

sub main {
	local *ARGV = \@_;
	select((select(STDERR),$|=1)[0]); # just in case
	Getopt::Long::Configure('bundling');
	GetOptions(
		'h' => sub {pod2usage(-verbose => 0, -exitval => 0)},
		'help' => sub {pod2usage(-verbose => 2, -exitval => 0)},
		'c' => \$opt_c,
		'd' => \$opt_d,
		'v' => \$opt_v,
		'x' => sub {$opt_x = 'x'},
		'X' => sub {$opt_x = 'X'},
	) && !@ARGV or pod2usage(-verbose => 0, -exitval => 2);
	$opt_x = 'x' if !defined($opt_x) && !defined($opt_d) && !defined($opt_c);
	$opt_x = '' if !defined($opt_x) && (defined($opt_d) || defined($opt_c));

	my $crc32 = crc32("", undef);

	binmode(STDIN);

	for (;;) {
		my $buf = '';
		my $bytes = sysread(STDIN, $buf, 32768);
		defined($bytes) or die "$me: error: failed reading input: $!\n";
		last if $bytes == 0;
		$crc32 = crc32($buf, $crc32);
	}

	my $ec = 0;
	my $tapline = "";
	if ($opt_c) {
		if ($crc32 == 0xFFFFFFFF) {
			$tapline = "ok - checksum good\n";
		} else {
			$ec = 1;
			$tapline = "not ok - checksum bad\n";
		}
		$opt_v or $tapline = "";
	}
	my $fmt = '%s';
	$fmt .= "%08$opt_x" if $opt_x;
	$fmt .= ($opt_x ? ' ' : '') . '%u' if $opt_d;
	$fmt .= "\n" if $opt_x || $opt_d;
	{
		no warnings;
		printf $fmt, $tapline, $crc32, $crc32;
	}

	exit($ec);
}

__DATA__

=head1 NAME

perlcrc32.pl - display CRC-32 checksum value

=head1 SYNOPSIS

B<perlcrc32.pl> [options]

 Options:
   -h                           show short usage help
   --help                       show long detailed help
   -c                           check the checksum is 0xffffffff
   -d                           show CRC-32 in decimal
   -v                           include TAP line with -c
   -x                           show CRC-32 in lowercase hexadecimal
   -X                           show CRC-32 in UPPERCASE hexadecimal

=head1 DESCRIPTION

B<perlcrc32.pl> computes the ISO CRC-32 value of standard input
and displays it as an 8-digit lowercase hexadecimal number (by
default) to standard output.

=head1 OPTIONS

=over

=item B<-c>

Verify that the computed checksum is 0xFFFFFFFF ("checksum good")
and set the exit status to 0 if it is or 1 if it's not ("checksum
bad").  Giving option B<-c> by itself suppresses other output by
default.

=item B<-d>

Show the computed CRC-32 value in decimal.  If this option is given,
then the decimal version of the computed CRC-32 value is always
output (preceded by the hexadecimal value and a space if the
hexadecimal value is also being output).

The checksum always appears on a separate, following line,
I<after> any "ok/not ok" line (as produced by B<-cv>).

The decimal value of the computed CRC-32 value is never output by
default.

=item B<-v>

Be verbose.  Currently the B<-v> option only affects the B<-c>
option.

If both B<-c> and B<-v> have been specified then, in addition to
setting the exit status, an S<"ok - checksum good"> or
S<"not ok - checksum bad"> line will be output to standard output
as the very first line (before the checksum itself).

=item B<-x>

Output the computed checksum using lowercase hexadecimal as exactly
8 hexadecimal digits at the very beginning of the line.

This is the default output unless B<-c>, B<-d> or B<-X> have been
given.  Giving an explicit option B<-x> will force output even with
B<-c> and supersedes any B<-X> option.

The checksum always appears on a separate, following line,
I<after> any "ok/not ok" line (as produced by B<-cv>).

=item B<-X>

Output the computed checksum using UPPERCASE hexadecimal as exactly
8 hexadecimal digits at the very beginning of the line.

The B<-X> option supersedes any B<-x> option.

The checksum always appears on a separate, following line,
I<after> any "ok/not ok" line (as produced by B<-cv>).

=back

=head1 DETAILS

The CRC-32 value being computed can be fully described as follows:

     CRC 32 polynomial: 0x04C11DB7
  initialization value: 0xFFFFFFFF
    bitwise endianness: little
   bit reversed result: yes
  xor final value with: 0xFFFFFFFF
  checksum "123456789": 0xCBF43926

This is the standard Ethernet/zlib CRC-32 computation and, in fact,
is performed by the zlib library's C<crc32> function courtesy of
the Compress::Zlib perl module.

Any output produced by perlcpio -c will result in a "good checksum"
result when using perlcrc32 -c.

=head1 LIMITATIONS

Requires the Compress::Zlib module to compute the CRC-32 value.

=head1 SEE ALSO

=over

=item Test Anything Protocol (TAP) specification (version 12)

=item L<https://testanything.org/tap-specification.html>

=back

=head1 COPYRIGHT AND LICENSE

=over

=item Copyright (C) 2020 Kyle J. McKay

=item All rights reserved.

=back

Permission to use, copy, modify, and distribute this software for any
purpose with or without fee is hereby granted, provided that the above
copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

=cut
