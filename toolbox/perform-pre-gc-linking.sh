#!/bin/sh

# Perform pre-gc linking of packs and objects to forks

# It may, under extremely unusual circumstances, be desirable to run git gc
# manually.  However, running git gc on a project that has forks is dangerous
# as it can reap objects not in use by the project itself but which are still
# in use by one or more forks which do not have their own copy since they use
# an alternates file to refer to them.
#
# Running this script on a project BEFORE manually running git gc on that
# project prevents this problem from occuring.
#
# Note that a .nogc file should really be created during the manual gc
# operation on a project!
#
# Alternatively, if a project is to be removed but its forks are to be kept
# then this script MUST be run before removing the project so as not to corrupt
# the forks that will be kept.
#
# Before the new order of gc came along this script did various things to
# try and optimize what it hard-linked down into the forks.  Now, however,
# it just hard-links all packs and loose objects down since with the new order
# both packs and objects could end up going away.
#
# This technique is not as optimal as the prior version, but with the advent
# of the new order for running gc and the ability to trigger/force a Girocco gc
# on a project using the "projtool.pl" command there's really no legitimate
# reason to use this script anymore other than when keeping the forks of a
# project while discarding the project itself.
#
# In that case all packs and objects must be hard-linked down to the child
# fork(s).  That functionality is all that remains in this script.  It accepts
# and ignores the previous options (for backwards compatibility) and just
# always does that now.
#
# The single mode of operation makes maintenance of this script easier too.

set -e

. @basedir@/shlib.sh

umask 002

force=
while case "$1" in
	--help|-h)
		cat <<EOT; exit 0;;
Usage: $(basename "$0") [option ...] <project-name>
       --force          Run even though no .nogc or .bypass file present
       --single-pack    Ignored for backwards compatibility
       --include-packs  Ignored for backwards compatibility
       <project-name>   Name of project (e.g. "git" or "git/fork" etc.)
Always hard-links all packs and all loose objects down to forks.
EOT
	--force)
		force=1;;
	--single-pack|--include-packs)
		;;
	--)
		shift; break;;
	-?*)
		echo "Unknown option: $1" >&2; exit 1;;
	*)
		! :;;
esac; do shift; done

proj="${1%.git}"
if [ "$#" -ne 1 ] || [ -z "$proj" ]; then
	echo "I need a project name (e.g. \"$(basename "$0") example\")"
	echo "(See also help -- \"$(basename "$0") --help\")"
	exit 1
fi
if ! cd "$cfg_reporoot/$proj.git"; then
	echo "no such directory: $cfg_reporoot/$proj.git"
	exit 1
fi
apid=
ahost=
{ read -r apid ahost ajunk <gc.pid; } >/dev/null 2>&1 || :
if [ -n "$apid" ] && [ -n "$ahost" ]; then
	echo "ERROR: refusing to run, $cfg_reporoot/$proj.git/gc.pid file exists"
	echo "ERROR: is gc already running on machine '$ahost' pid '$apid'?"
	exit 1
fi

if [ -z "$force" ] && ! [ -e .nogc ] && ! [ -e .bypass ]; then
	echo "WARNING: no .nogc or .bypass file found in $cfg_reporoot/$proj.git"
	echo "WARNING: jobd.pl could run gc.sh while you're fussing with $proj"
	echo "WARNING: either create one of those files or re-run with --force"
	echo "WARNING: (e.g. \"$(basename "$0") --force ${singlepack:+--single-pack }$proj\") to bypass this warning"
	echo "WARNING: please remember to remove the file after you're done fussing"
	exit 1
fi

# date -R is linux-only, POSIX equivalent is '+%a, %d %b %Y %T %z'
datefmt='+%a, %d %b %Y %T %z'

trap 'echo "hard-linking failed" >&2; exit 1' EXIT

if has_forks_with_alternates "$proj"; then

	# We have to update the lastparentgc time in the child forks even if they do not get any
	# new "loose objects" because they need to run gc just in case the parent now has some
	# objects that used to only be in the child so they can be removed from the child.
	# For example, a "patch" might be developed first in a fork and then later accepted into
	# the parent in which case the objects making up the patch in the child fork are now
	# redundant (since they're now in the parent as well) and need to be removed from the
	# child fork which can only happen if the child fork runs gc.

	# It is enough to copy objects just one level down and get_repo_list
	# takes a regular expression (which is automatically prefixed with '^')
	# so we can easily match forks exactly one level down from this project

	forkdir="$proj"
	get_repo_list "$forkdir/[^/:][^/:]*:" |
	while read fork; do
		# Ignore forks that do not exist or are symbolic links
		! [ -L "$cfg_reporoot/$fork.git" ] && [ -d "$cfg_reporoot/$fork.git" ] ||
			continue
		# Or have an empty alternates file
		! is_empty_alternates_file "$cfg_reporoot/$fork.git/objects/info/alternates" ||
			continue
		# Match objects in parent project
		for d in objects/$octet; do
			[ "$d" != "objects/$octet" ] || continue
			mkdir -p "$cfg_reporoot/$fork.git/$d"
			find -L "$d" -maxdepth 1 -type f -name "$octet19*" -exec \
				"$var_sh_bin" -c 'ln -f "$@" '"'$cfg_reporoot/$fork.git/$d/'" sh '{}' + || :
		done
		# Match packs in parent project
		mkdir -p "$cfg_reporoot/$fork.git/objects/pack"
		list_packs --all --exclude-no-idx objects/pack | LC_All=C sed 'p;s/\.pack$/.idx/' |
			xargs "$var_sh_bin" -c 'ln -f "$@" '"'$cfg_reporoot/$fork.git/objects/pack/'" sh || :
		if ! [ -e "$cfg_reporoot/$fork.git/.needsgc" ]; then
			# Trigger a mini gc in the fork if it now has too many packs
			packs="$(list_packs --quiet --count --exclude-no-idx --exclude-keep "$cfg_reporoot/$fork.git/objects/pack")" || :
			if [ -n "$packs" ] && [ "$packs" -ge 20 ]; then
				>"$cfg_reporoot/$fork.git/.needsgc"
			fi
		fi
		git --git-dir="$cfg_reporoot/$fork.git" update-server-info
		# Update the fork's lastparentgc date
		git --git-dir="$cfg_reporoot/$fork.git" config gitweb.lastparentgc "$(date "$datefmt")"
	done
fi

trap - EXIT
echo "packs and loose objects for $proj have now been linked into child forks (if any)"
