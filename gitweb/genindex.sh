#!/bin/sh
#
# genindex - Generate gitweb project list from Girocco's

# Usage: genindex.sh [project-to-update]
#
# If project-to-update is given, then only that one will be updated

. @basedir@/shlib.sh

set -e

update="${1%.git}"

# Use the correct umask so the list file is group-writable, if owning_group set
if [ -n "$cfg_owning_group" ]; then
	umask 002
fi

# gitweb calls CGI::Util::unescape on both the path and owner, but the only
# character we allow that needs to be escaped is '+' which is allowed in
# both the owner email and in the project name.  Otherwise '+' will be
# displayed as a ' ' in the owner email and will cause a project name
# containing it to be omitted from the project list page.

GIROCCO_REPOROOT="$cfg_reporoot" && export GIROCCO_REPOROOT
if [ -z "$update" ] || [ ! -s "$cfg_projlist_cache_dir/gitproj.list" ]; then
	# Must read all the owners so don't bother with join at all
	get_repo_list |
	perl -I@basedir@ -MDigest::MD5=md5_hex -MCwd=realpath -MGirocco::ConfigUtil \
		-n -e 'BEGIN{$reporoot=$ENV{GIROCCO_REPOROOT};}' 2>/tmp/gitdir.listu.$$ -e \
		'chomp; my $p = $_; defined($p) && $p ne "" or next; my $o = "";
		my $cf = read_config_file_hash("$reporoot/$p.git/config");
		defined($cf->{"gitweb.owner"}) and $o = $cf->{"gitweb.owner"};
		print "$p ",md5_hex(lc($o))," $o\n";
		my $rp = realpath("$reporoot/$p.git");
		defined($rp) && $rp ne "" and print STDERR "$p $rp\n";' |
	LC_ALL=C sort -k 1,1 >/tmp/gitproj.list.$$
	test $? -eq 0
	LC_ALL=C sort -k 1,1 </tmp/gitdir.listu.$$ >/tmp/gitdir.list.$$
	rm -f /tmp/gitdir.listu.$$
else
	GIROCCO_UPDATE="$update" && export GIROCCO_UPDATE
	get_repo_list | LC_ALL=C sort -k 1,1 >/tmp/gitproj.srt.$$
	LC_ALL=C join -a 1 /tmp/gitproj.srt.$$ "$cfg_projlist_cache_dir/gitproj.list" |
	perl -I@basedir@ -MDigest::MD5=md5_hex -MGirocco::ConfigUtil \
		-n -e 'BEGIN{$reporoot=$ENV{GIROCCO_REPOROOT};$update=$ENV{GIROCCO_UPDATE};}' -e \
		'BEGIN{$mt5=md5_hex("");}
		chomp; my @f=split(" ",$_,3); push(@f, "") while @f < 3; $f[0] ne "" or next;
		my $r = $f[0] eq $update || $f[1] eq "" || ($f[2] eq "" && $f[1] ne $mt5);
		if ($r) {
			my $o = "";
			my $cf = read_config_file_hash("$reporoot/$f[0].git/config");
			defined($cf->{"gitweb.owner"}) and $o = $cf->{"gitweb.owner"};
			$f[1] = md5_hex(lc($o)); $f[2]=$o;
		}
		print "$f[0] $f[1] $f[2]\n";' |
	LC_ALL=C sort -k 1,1 >/tmp/gitproj.list.$$
	test $? -eq 0
	LC_ALL=C join -a 1 -t ' ' /tmp/gitproj.srt.$$ "$cfg_projlist_cache_dir/gitdir.list" |
	perl -MCwd=realpath \
		-n -e 'BEGIN{$reporoot=$ENV{GIROCCO_REPOROOT};$update=$ENV{GIROCCO_UPDATE};}' -e \
		'chomp; my @f=split(" ",$_,2); push(@f, "") while @f < 2; $f[0] ne "" or next;
		my $r = $f[0] eq $update || $f[1] eq "";
		$r and $f[1] = realpath("$reporoot/$f[0].git");
		defined($f[1]) && $f[1] ne "" and print "$f[0] $f[1]\n";' |
	LC_ALL=C sort -k 1,1 >/tmp/gitdir.list.$$
	test $? -eq 0
	rm -f /tmp/gitproj.srt.$$
fi
cut -d ' ' -f 1,3- </tmp/gitproj.list.$$ | sed -e 's/ /.git /;s/+/%2B/g' >/tmp/gitweb.list.$$

# Make sure we are on the correct device before the atomic move
rm -f \
	"$cfg_projlist_cache_dir/gitproj.list.$$" \
	"$cfg_projlist_cache_dir/gitdir.list.$$" \
	"$cfg_projlist_cache_dir/gitweb.list.$$"
cat /tmp/gitproj.list.$$ >"$cfg_projlist_cache_dir/gitproj.list.$$"
cat /tmp/gitdir.list.$$ >"$cfg_projlist_cache_dir/gitdir.list.$$"
cat /tmp/gitweb.list.$$ >"$cfg_projlist_cache_dir/gitweb.list.$$"
rm -f /tmp/gitproj.list.$$ /tmp/gitdir.list.$$ /tmp/gitweb.list.$$

# Set the proper group, if configured, before the move
if [ -n "$cfg_owning_group" ]; then
	chgrp "$cfg_owning_group" \
	"$cfg_projlist_cache_dir/gitproj.list.$$" \
	"$cfg_projlist_cache_dir/gitdir.list.$$" \
	"$cfg_projlist_cache_dir/gitweb.list.$$"
fi

# Atomically move into place
mv -f "$cfg_projlist_cache_dir/gitproj.list.$$" "$cfg_projlist_cache_dir/gitproj.list"
mv -f "$cfg_projlist_cache_dir/gitdir.list.$$" "$cfg_projlist_cache_dir/gitdir.list"
mv -f "$cfg_projlist_cache_dir/gitweb.list.$$" "$cfg_projlist_cache_dir/gitweb.list"
