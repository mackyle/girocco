# makefile -- redirect "make" command to version specific make
# Copyright (C) 2015 Kyle J. McKay.  All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its contributors
#    may be used to endorse or promote products derived from this software
#    without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#
#
# Note that .POSIX must be the first non-comment line and
# also that the behavior of blank lines is unspecified
.POSIX:
#
# Set MAKEFILE__NAME to the version-specific file name that "make" would
# read instead of this file if the correct version of "make" had been run.
# For GNU make use "GNUmakefile" and for BSD make use "BSDmakefile".
# For kmk use either "Makefile.kmk" or "makefile.kmk"
MAKEFILE__NAME = GNUmakefile
#
# Set MAKE__COMMAND__NAME to the typical name of the version-specific "make"
# command that was intended to be run when it's not available as "make".
# For GNU make this is typically "gmake" and for BSD make typically "bsdmake".
# For kmk this should be "kmk".
MAKE__COMMAND__NAME = gmake
#
# Note that when we run $(MAKE__COMMAND__NAME) we do not need to pass on any
# "make" options as a POSIX "make" will have put them in MAKEFLAGS for us
# and $(MAKE__COMMAND__NAME) will pick them up from there.
#
# In case any of the other targets are not recognized as special, the default
# target must be the first target and it depends on a file that should not exist
# just in case .PHONY is not supported by the "make" that's reading this file.
# This rule will be used if no targets are specified on the command line.  We
# must not name it "all" because we do not pass the target name on in this case
# -- that is handled by the .DEFAULT rule.
__makefile_compatibility_wrapper__: __file_which_should_not_exist
	+@test -r "$(MAKEFILE__NAME)" || \
	{ echo error: missing $(MAKEFILE__NAME); exit 1; }
	+@$(MAKE__COMMAND__NAME)
#
# Turn off parallel mode on "make"s that support it
# Other "makes" will treat these as normal rules
.NOTPARALLEL:
.NO_PARALLEL:
#
# Ensure that there are no built-in rules to interfere
# with using our .DEFAULT rule
.SUFFIXES:
#
# This rule will be used if any targets are specified
.DEFAULT:
	+@test -r "$(MAKEFILE__NAME)" || \
	{ echo error: missing $(MAKEFILE__NAME); exit 1; }
	+@$(MAKE__COMMAND__NAME) $@
# This rule shouldn't be necessary but some makes are broken
install: __file_which_should_not_exist
	+@test -r "$(MAKEFILE__NAME)" || \
	{ echo error: missing $(MAKEFILE__NAME); exit 1; }
	+@$(MAKE__COMMAND__NAME) install
#
# Mark "__file_which_should_not_exist" as a phony target
# for "make"s which support phony targets
.PHONY: __file_which_should_not_exist
#
# In case .PHONY is not supported we need an explicit
# rule for this to make sure the .DEFAULT rule is not used
# and we end it with ; to make it clear it has no commands
__file_which_should_not_exist: ;
