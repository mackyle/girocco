#!/bin/sh

# License: public domain -or- http://www.wtfpl.net/txt/copying/

die() { >&2 printf 'Bail out! %s\n' "$*"; exit 1; }

testnum=0

# $1 => value to hash (may contain escapes) '-' reads stdin
# $2 => lowercase sha256 hash expected
# $3... => test name
test_sha256() {
	_val="$1"; shift
	_hash="$1"; shift
	_test="$*"
	if [ "$_val" = "-" ]; then
		_check="$(./ltsha256)"
	else
		_check="$(printf '%b' "$_val" | ./ltsha256)"
	fi ||
	die "could not run ltsha256 utility"
	_result="ok"
	[ "$_check" = "$_hash" ] || _result="not ok"
	testnum="$(( $testnum + 1 ))"
	printf '%s %u - %s\n' "$_result" "$testnum" "$_test"
}

echo '1..6'

test_sha256 \
	"abc" \
	ba7816bf8f01cfea414140de5dae2223b00361a396177a9cb410ff61f20015ad \
	abc

test_sha256 \
	"abcdbcdecdefdefgefghfghighijhijkijkljklmklmnlmnomnopnopq" \
	248d6a61d20638b8e5c026930c3e6039a33ce45964ff2167f6ecedd419db06c1 \
	abcdbcdecdefdefgefghfghighijhijkijkljklmklmnlmnomnopnopq

test_sha256 \
	"123456789" \
	15e2b0d3c33891ebb0f1ef609ec419420c20e320ce94c65fbc8c3312448eb225 \
	123456789

test_sha256 \
	"The quick brown fox jumped over the lazy dog.\n" \
	40d5c6f7fe5672fb52269f651c2b985867dfcfa4a5c5258e3d4f8736d5095037 \
	"quick brown fox"

test_sha256 \
	- <gitblob.bin \
	473a0f4c3be8a93681a267e3b1e9a7dcda1185436fe141f7749120a303721813 \
	"git sha256 empty blob"

test_sha256 \
	- <gittree.bin \
	6ef19b41225c5369f1c104d45d8d85efa9b057b53b14b4b9b939dd74decc5321 \
	"git sha256 empty tree"

exit 0
