/* License: public domain -or- http://www.wtfpl.net/txt/copying/ */

#include "lt256.h"
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <unistd.h>

#define READSIZE 32768

static const char hextab[16] = {
	'0', '1', '2', '3', '4', '5', '6', '7',
	'8', '9', 'a', 'b', 'c', 'd', 'e', 'f'
};
static char buffer[READSIZE];

int main(int arc, char *argv[])
{
	SHA256_CTX c;
	unsigned char md[SHA256_DIGEST_LENGTH];
	char mdhex[(2*SHA256_DIGEST_LENGTH)+1];
	ssize_t e;
	unsigned i;
	
	if (!SHA256_Init(&c))
		return EXIT_FAILURE;

	for (;;) {
		do {
			e = read(STDIN_FILENO, buffer, READSIZE);
		} while (e == -1 &&
			(errno == EINTR || errno == EAGAIN || errno == EWOULDBLOCK));
		if (e < 0)
			return EXIT_FAILURE;
		if (!e)
			break;
		if (!SHA256_Update(&c, buffer, (size_t)e))
			return EXIT_FAILURE;
	}
	if (!SHA256_Final(md, &c))
		return 0;
	for (i=0; i < SHA256_DIGEST_LENGTH; ++i) {
		unsigned char c = md[i];
		mdhex[i<<1] = hextab[c >> 4];
		mdhex[(i<<1)+1] = hextab[c & 0xf];
	}
	mdhex[2*SHA256_DIGEST_LENGTH] = '\0';
	if (puts(mdhex) < 0)
		return EXIT_FAILURE;

	return 0;
}

#include "lt256.c"
