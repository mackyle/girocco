/*

list_packs.c -- list_packs utility to count Git packs and their objects
Copyright (C) 2016,2017 Kyle J. McKay.
All rights reserved.

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

*/

/*
  This utility is intended to be used by a script to assist in determining
  whether or not it's time to run gc in the case where gc.auto=0 and when
  it is to provide a convenient mechanism to feed selected pack names to
  a script for futher processing at gc time.

  Various options are available to select which .pack files to inspect
  including supplying the names.  This utility is intended to be able
  to read the pack names from the --export-pack-edges file that may be
  produced by git fast-import without needing any preprocessing.

  See the list_packs.txt file or run the command with no arguments for help.
*/

#define _XOPEN_SOURCE 600
#undef _FILE_OFFSET_BITS
#define _FILE_OFFSET_BITS 64
#include <stdarg.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h> /* in some cases required before dirent.h or sys/stat.h */
#include <dirent.h>
#include <inttypes.h>
#include <limits.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <sys/stat.h>

#ifndef PATH_MAX
#define PATH_MAX 1024
#endif
#if PATH_MAX < 4096
#define PATH_BUFF 4096
#else
#define PATH_BUFF PATH_MAX
#endif

static int fgetfn(FILE *f, char *b, size_t s);
static void process_pack_filename(char *fn, size_t bl, int hasps);
static void process_pack(const char *fn, uint32_t objcnt);
static void process_packs_finish(void);

struct pack_info {
	struct pack_info *next;
	uint32_t objcount;
	char filename[1];
};

struct ext_info {
	struct ext_info *next;
	size_t extlen; /* strlen(ext) */
	char ext[12]; /* includes leading '.' and trailing '\0' */
	int xxt; /* >0 excludes if ext present, 0 excludes if ext NOT present */
};

#define MAX_EXT_LEN (sizeof(((const struct ext_info *)0)->ext) - 2)
#define MAX_SFX_LEN (sizeof(((const struct ext_info *)0)->ext) - 1)
#define BAD_EXT_CHARS ":./\\ \t\n\v\f\r"

const char USAGE[] =
#include "list_packs.inc"
;

static int opt_q = 0;
static int opt_xix = -1;
static long opt_limit = 0;
static int opt_count = 0;
static uint64_t count = 0;
static uint64_t objlimit = 0;
static int opt_desc = 0;
static int opt_boundary = 0;
static uint64_t maxlimit = 0;
static uint64_t processed = 0;

static struct pack_info *packlist = NULL;
static size_t packcount = 0;

static struct ext_info *extlist = NULL;
static struct ext_info *sfxlist = NULL;

static char fnbuff[PATH_BUFF];

static void die(const char *fmt, ...)
{
	va_list args;
	va_start(args, fmt);
	fflush(stdout);
	if (!opt_q)
		vfprintf(stderr, fmt, args);
	va_end(args);
	exit(EXIT_FAILURE);
}

static void dienomem(const char *what)
{
	if (what && !*what)
		what = NULL;
	die("list_packs: error: out of memory%s%s\n", (what ? " for " : ""),
		(what ? what : ""));
}

static void dienopackmem(void)
{
	dienomem("pack list");
}

static void dienoextmem(void)
{
	dienomem("ext list");
}

static void dieusage(int err)
{
	FILE *f = err ? stderr : stdout;
	fflush(stdout);
	if (!err || !opt_q)
		fprintf(f, "%s", USAGE);
	exit(err);
}

static int has_suffix(const char *s, size_t l, const char *x, size_t b)
{
	if (!s || !x || !b || l < (b + 1) /* ?<suffix> */)
		return 0;
	return !strncmp(s + l - b, x, b);
}
#define has_idx_suffix(s,l) has_suffix((s),(l),".idx",4)
#define has_pack_suffix(s,l) has_suffix((s),(l),".pack",5)

static int is_pack_sha1_name(const char *s, size_t l)
{
	if (!s || l < 50)
		return 0;
	if (strncmp(s, "pack-", 5) || strncmp(s + l - 5, ".pack", 5))
		return 0;
	return strspn(s + 5, "0123456789abcdefABCDEF") >= 40;
}

#define find_add_ext(ext) find_add_extsfx(&extlist, ext, '.')
#define find_add_sfx(sfx) find_add_extsfx(&sfxlist, sfx, 0)
static struct ext_info *find_add_extsfx(struct ext_info **list, const char *extsfx, char ch)
{
	size_t elen = strlen(extsfx);
	size_t b = ch ? 1 : 0;
	struct ext_info *result = *list;

	while (result && strcmp(&result->ext[b], extsfx)) {
		result = result->next;
	}
	if (!result) {
		result = (struct ext_info *)malloc(sizeof(struct ext_info));
		if (!result)
			dienoextmem();
		result->extlen = elen + b;
		if (b)
			result->ext[0] = ch;
		memcpy(&result->ext[b], extsfx, elen + 1);
		if (elen + b + 1 < sizeof(result->ext))
			memset(&result->ext[elen + b + 1], 0,
				sizeof(result->ext) - (elen + b + 1));
		result->xxt = -1;
		result->next = *list;
		*list = result;
	}
	return result;
}

void handle_ext_option(const char *ext, int v)
{
	size_t elen;
	struct ext_info *einfo;

	if (!ext || !*ext || !(elen = strlen(ext)) ||
	    elen > MAX_EXT_LEN || strcspn(ext, BAD_EXT_CHARS) != elen)
		dieusage(EXIT_FAILURE);
	if (!strcmp(ext, "idx")) {
		opt_xix = v;
	} else {
		einfo = find_add_ext(ext);
		einfo->xxt = v;
	}
}

void handle_sfx_option(const char *sfx, int v)
{
	size_t elen;
	struct ext_info *sinfo;

	if (!sfx || !*sfx || !(elen = strlen(sfx)) ||
	    elen > MAX_SFX_LEN || strcspn(sfx, BAD_EXT_CHARS) != elen ||
	    strchr("0123456789abcdefABCDEF", sfx[0]))
		dieusage(EXIT_FAILURE);
	sinfo = find_add_sfx(sfx);
	sinfo->xxt = v;
}

int main(int argc, char *argv[])
{
	int argn;
	int opt_a = 0;
	const char *only = NULL;
	const char *dir = NULL;
	int is_stdin = 0;
	FILE *in = NULL;

	for (argn = 1; argn < argc; ++argn) {
		if (!strcmp(argv[argn], "-h") || !strcmp(argv[argn], "--help")) {
			dieusage(EXIT_SUCCESS);
		} else if (!strcmp(argv[argn], "-q") || !strcmp(argv[argn], "--quiet")) {
			opt_q = 1;
		} else if (!strcmp(argv[argn], "-a") || !strcmp(argv[argn], "--all")) {
			opt_a = 1;
		} else if (!strcmp(argv[argn], "--exclude-idx")) {
			opt_xix = 1;
		} else if (!strcmp(argv[argn], "--exclude-no-idx")) {
			opt_xix = 0;
		} else if (!strcmp(argv[argn], "--exclude-keep")) {
			handle_ext_option("keep", 1);
		} else if (!strcmp(argv[argn], "--exclude-no-keep")) {
			handle_ext_option("keep", 0);
		} else if (!strcmp(argv[argn], "--exclude-bitmap")) {
			handle_ext_option("bitmap", 1);
		} else if (!strcmp(argv[argn], "--exclude-no-bitmap")) {
			handle_ext_option("bitmap", 0);
		} else if (!strcmp(argv[argn], "--exclude-bndl")) {
			handle_ext_option("bndl", 1);
		} else if (!strcmp(argv[argn], "--exclude-no-bndl")) {
			handle_ext_option("bndl", 0);
		} else if (!strcmp(argv[argn], "--count")) {
			opt_count = 1;
		} else if (!strcmp(argv[argn], "--count-objects")) {
			opt_count = 2;
		} else if (!strcmp(argv[argn], "--include-boundary")) {
			opt_boundary = 1;
		} else if (!strcmp(argv[argn], "--exclude-ext")) {
			if (++argn >= argc)
				dieusage(EXIT_FAILURE);
			handle_ext_option(argv[argn], 1);
		} else if (!strcmp(argv[argn], "--exclude-no-ext")) {
			if (++argn >= argc)
				dieusage(EXIT_FAILURE);
			handle_ext_option(argv[argn], 0);
		} else if (!strcmp(argv[argn], "--exclude-sfx")) {
			if (++argn >= argc)
				dieusage(EXIT_FAILURE);
			handle_sfx_option(argv[argn], 1);
		} else if (!strcmp(argv[argn], "--exclude-no-sfx")) {
			if (++argn >= argc)
				dieusage(EXIT_FAILURE);
			handle_sfx_option(argv[argn], 0);
		} else if (!strcmp(argv[argn], "--exclude-limit")) {
			char *end;
			long limit = 0;

			if (++argn >= argc)
				dieusage(EXIT_FAILURE);
			limit = strtol(argv[argn], &end, 0);
			if (!*argv[argn] || *end || !limit)
				dieusage(EXIT_FAILURE);
			opt_limit = limit;
		} else if (!strcmp(argv[argn], "--object-limit")) {
			char *end;
			long limit = 0;

			if (++argn >= argc)
				dieusage(EXIT_FAILURE);
			limit = strtol(argv[argn], &end, 0);
			if (!*argv[argn] || *end || !limit)
				dieusage(EXIT_FAILURE);
			if (limit < 0) {
				opt_desc = 1;
				objlimit = (uint64_t)-limit;
			} else {
				objlimit = (uint64_t)limit;
			}
		} else if (!strcmp(argv[argn], "--max-matches")) {
			char *end;
			long limit = 0;

			if (++argn >= argc)
				dieusage(EXIT_FAILURE);
			limit = strtol(argv[argn], &end, 0);
			if (!*argv[argn] || *end || limit <= 0)
				dieusage(EXIT_FAILURE);
			maxlimit = (uint64_t)limit;
		} else if (!strcmp(argv[argn], "--only")) {
			if (++argn >= argc || !*argv[argn])
				dieusage(EXIT_FAILURE);
			only = argv[argn];
		} else if (!strcmp(argv[argn], "-C")) {
			if (++argn >= argc || !*argv[argn])
				dieusage(EXIT_FAILURE);
			if (chdir(argv[argn])) {
				if (!opt_q)
					fprintf(stderr, "list_packs: error: "
						"chdir '%s' failed\n", argv[argn]);
				exit(EXIT_FAILURE);
			}
		} else if (!strcmp(argv[argn], "--")) {
			++argn;
			break;
		} else if (argv[argn][0] == '-' && argv[argn][1]) {
			dieusage(EXIT_FAILURE);
		} else {
			break;
		}
	}
	if (argn < argc && *argv[argn])
		dir = argv[argn++];
	if (argn != argc || (!only && !dir) || (only && dir) || (only && opt_a))
		dieusage(EXIT_FAILURE);
	if (only) {
		if (!strcmp(only, "-")) {
			is_stdin = 1;
			in = stdin;
		} else {
			in = fopen(only, "r");
			if (!in)
				die("list_packs: error: could not open %s\n", only);
		}
		while (fgetfn(in, fnbuff, sizeof(fnbuff) - (MAX_EXT_LEN + 1))) {
			char *fn = fnbuff;
			size_t l = strlen(fn);
			int ips;

			if (!l)
				continue;
			if (l > 2 && !strncmp(fn, "./", 2)) {
				fn += 2;
				l -= 2;
			}
			ips = has_pack_suffix(fn, l);
			process_pack_filename(fn, (ips ? l - 5 : l), ips);
		}
		if (!is_stdin)
			fclose(in);
	} else {
		size_t l;
		DIR *d;
		struct dirent *e;

		l = strlen(dir);
		while (l > 1 && dir[l-1] == '/') {
			--l;
		}
		if (l > 2 && !strncmp(dir, "./", 2)) {
			dir += 2;
			l -= 2;
		}
		if (l + 10 /* "/?.bitmap\0" */ > PATH_BUFF)
			die("list_packs: error: dirname too long\n");
		memcpy(fnbuff, dir, l);
		fnbuff[l] = '\0';
		d = opendir(fnbuff);
		if (!d)
			die("list_packs: error: could not read directory %s\n", fnbuff);
		if (!strcmp(fnbuff, ".")) {
			l = 0;
			fnbuff[0] = '\0';
		}
		if (l && fnbuff[l-1] != '/')
			fnbuff[l++] = '/';
		while ((e = readdir(d)) != NULL) {
			/* d_namlen is a nice, but non-POSIX extension */
			size_t el = strlen(e->d_name);

			if (has_pack_suffix(e->d_name, el) &&
			    (opt_a || is_pack_sha1_name(e->d_name, el))) {
				if (l + el + 3 /* "ap\0" */ > PATH_BUFF) {
					if (!opt_q)
						fprintf(stderr, "list_packs: warning: "
							"ignored input filename greater "
							"than %d characters long\n",
							PATH_BUFF - 3);
					continue;
				}
				memcpy(fnbuff + l, e->d_name, el + 1 /* \0 */);
				process_pack_filename(fnbuff, l + el - 5 /* .pack */, 1);
			}
		}
		closedir(d);
	}
	process_packs_finish();
	if (opt_count)
		printf("%"PRIu64"\n", count);

	return EXIT_SUCCESS;
}

#define FNDELIM "\t\n\v\f\r :"

static int fgetfn(FILE *f, char *b, size_t s)
{
	size_t l, fnl;
	int trunc;

	if (!fgets(b, (int)s, f)) {
		if (ferror(f)) {
			if (!opt_q)
				fprintf(stderr, "list_packs: error: an error "
					"occurred reading pack name list file\n");
			exit(EXIT_FAILURE);
		}
		return 0;
	}
	if (!*b)
		return 1;
	l = strlen(b);
	fnl = strcspn(b, FNDELIM);
	if (b[l-1] != '\n' && !feof(f)) {
		int ch;
		flockfile(f);
		while ((ch = getc_unlocked(f)) != EOF && ch != '\n') {
			/* loop */
		}
		funlockfile(f);
		trunc = 1;
	} else {
		trunc = 0;
	}
	if (fnl < l || (!ferror(f) && !trunc)) {
		b[fnl] = '\0';
		return 1;
	}
	if (ferror(f)) {
		if (!opt_q)
			fprintf(stderr, "list_packs: error: an error "
				"occurred reading pack name list file\n");
		exit(EXIT_FAILURE);
	}
	if (!opt_q)
		fprintf(stderr, "list_packs: warning: ignored input filename "
			"greater than %d characters long\n", (int)s - 2);
	*b = '\0';
	return 1;
}

static int file_exists(const char *fn, struct stat *s)
{
	if (!stat(fn, s)) {
		if (S_ISREG(s->st_mode))
			return 1;
		if (!opt_q)
			fprintf(stderr, "list_packs: warning: ignoring "
				"non-file '%s'\n", fn);
	}
	return 0;
}

static void process_pack_filename(char *fn, size_t bl, int hasps)
{
	struct stat ps, es;
	FILE *f;
	union {
		uint32_t u[3];
		char c[12];
	} hdr;
	uint32_t packver;
	uint32_t objcnt;
	const struct ext_info *einfo;
	int sfxor;

	if (stat(fn, &ps) || !S_ISREG(ps.st_mode)) {
		if (!opt_q)
			fprintf(stderr, "list_packs: warning: ignoring "
				"non-file '%s'\n", fn);
		return;
	}
	if (ps.st_size < 32) {
		if (!opt_q)
			fprintf(stderr, "list_packs: warning: ignoring "
				"invalid pack file '%s'\n", fn);
		return;
	}
	einfo = sfxlist;
	sfxor = -1;
	while (einfo) {
		if (einfo->xxt >= 0) {
			int hsfx;

			hsfx = (bl >= einfo->extlen) &&
				!strncmp(fn + bl - einfo->extlen, einfo->ext, einfo->extlen);
			if (einfo->xxt) {
				if (hsfx)
					return;
			} else if ((sfxor = hsfx)) {
					break;
			}
		}
		einfo = einfo->next;
	}
	if (!sfxor)
		return;
	einfo = extlist;
	while (einfo) {
		if (einfo->xxt >= 0) {
			int hext;

			memcpy(fn + bl, einfo->ext, einfo->extlen + 1);
			hext = file_exists(fn, &es);
			if ((einfo->xxt && hext) || (!einfo->xxt && !hext))
				return;
		}
		einfo = einfo->next;
	}
	if (opt_xix >= 0) {
		int hx;

		memcpy(fn + bl, ".idx", 5);
		hx = file_exists(fn, &es);
		if ((opt_xix && hx) || (!opt_xix && !hx))
			return;
	}
	if (hasps)
		memcpy(fn + bl, ".pack", 6);
	else
		fn[bl] = '\0';
	f = fopen(fn, "rb");
	if (!f) {
		if (!opt_q)
			fprintf(stderr, "list_packs: warning: ignoring "
				"unopenable pack file '%s'\n", fn);
		return;
	}
	if (fread(&hdr, 12, 1, f) != 1) {
		fclose(f);
		if (!opt_q)
			fprintf(stderr, "list_packs: warning: ignoring "
				"unreadable pack file '%s'\n", fn);
		return;
	}
	fclose(f);
	packver = ntohl(hdr.u[1]);
	objcnt = ntohl(hdr.u[2]);
	if (memcmp(hdr.c, "PACK", 4) || (packver != 2 && packver != 3) ||
	    ps.st_size < ((off_t)objcnt + 32)) {
		if (!opt_q)
			fprintf(stderr, "list_packs: warning: ignoring "
				"invalid pack file '%s'\n", fn);
		return;
	}
	if (!opt_xix && es.st_size < ((off_t)objcnt * 28 + 1072)) {
		if (!opt_q)
			fprintf(stderr, "list_packs: warning: ignoring pack "
				"with invalid idx file '%.*s.idx'\n", (int)bl, fn);
		return;
	}
	if (opt_limit) {
		if ((opt_limit > 0 && objcnt >= (uint32_t)opt_limit) ||
		    (opt_limit < 0 && objcnt < (uint32_t)-opt_limit))
			return;
	}
	/* the PACK file passed all the checks, process it */
	if (objlimit) {
		size_t fnlen = strlen(fn);
		struct pack_info *info = (struct pack_info *)
			malloc(sizeof(struct pack_info) + fnlen);

		if (!info)
			dienopackmem();
		info->objcount = objcnt;
		memcpy(info->filename, fn, fnlen + 1);
		info->next = packlist;
		packlist = info;
		++packcount;
	} else {
		process_pack(fn, objcnt);
	}
}

static void process_pack(const char *fn, uint32_t objcnt)
{
	if (maxlimit && processed >= maxlimit)
		return;
	if (opt_count) {
		if (opt_count > 1)
			count += objcnt;
		else
			++count;
	} else {
		printf("%s\n", fn);
	}
	++processed;
}

static void process_pack_info(const struct pack_info *pack)
{
	process_pack(pack->filename, pack->objcount);
}

static int sort_asc(const void *p, const void *q)
{
	const struct pack_info **a = (const struct pack_info **)p;
	const struct pack_info **b = (const struct pack_info **)q;
	if ((*a)->objcount < (*b)->objcount)
		return -1;
	if ((*a)->objcount > (*b)->objcount)
		return 1;
	return strcmp((*a)->filename, (*b)->filename);
}

static int sort_dsc(const void *p, const void *q)
{
	return sort_asc(q, p);
}

static void process_packs_finish(void)
{
	struct pack_info **table, *p;
	size_t i;
	uint64_t tally;

	if (!objlimit || !packlist || !packcount)
		return;
	table = (struct pack_info **)malloc(sizeof(struct pack_info *) * packcount);
	if (!table)
		dienopackmem();
	i = 0;
	p = packlist;
	do {
		table[i++] = p;
		p = p->next;
	} while (p);
	qsort(table, packcount, sizeof(struct pack_info *), (opt_desc ? sort_dsc : sort_asc));
	tally = 0;
	for (i=0; i < packcount; ++i) {
		tally += table[i]->objcount;
		if (tally <= objlimit) {
			process_pack_info(table[i]);
		} else {
			if (opt_boundary)
				process_pack_info(table[i]);
			break;
		}
	}
}
