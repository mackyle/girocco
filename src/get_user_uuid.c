/*

get_user_uuid.c -- get user UUID from passwd file
Copyright (c) 2014 Kyle J. McKay.  All rights reserved.

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

*/

/*
  USAGE: get_user_uuid username
  Exit code 0 for yes, non-zero for no
  If username has a UUID returns yes and echos it to stdout
  Wrong number of or bad arguments exits with non-zero
  Directly reads compiled in PASSWD_FILE location
*/

#ifndef PASSWD_FILE
#error PASSWD_FILE must be defined to a string which is the full pathname to\
 the /etc/passwd format file to read
#endif

#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#define BADCHARS ":,# \t\r\n"
#define MAX_LINE_SIZE 16384 /* includes trailing \n and \0 */
#define MAX_NAME_LEN 64

static char linebuffer[MAX_LINE_SIZE];

int is_valid_name(const char *name)
{
  size_t len;

  if (!name || !*name)
    return 0;
  len = strlen(name);
  if (len > MAX_NAME_LEN || strcspn(name, BADCHARS) != len)
    return 0;
  return 1;
}

int main(int argc, char *argv[])
{
  FILE *passwdfile;
  size_t len;

  if (argc != 2)
    return EXIT_FAILURE;
  if (!is_valid_name(argv[1]))
    return EXIT_FAILURE;

  len = strlen(argv[2]);

  passwdfile = fopen(PASSWD_FILE, "r");
  if (!passwdfile)
    return EXIT_FAILURE;
  while (fgets(linebuffer, sizeof(linebuffer)-1, passwdfile)) {
    char *trimmed, *uuid;
    size_t len = strlen(linebuffer);

    if (len >= sizeof(linebuffer) - 2)
      return EXIT_FAILURE;
    if (len && linebuffer[len-1] == '\n')
      linebuffer[--len] = '\0';
    trimmed = linebuffer + strspn(linebuffer, " \t");
    if (!*trimmed || *trimmed == '#')
      continue;
    if (trimmed != linebuffer)
      return EXIT_FAILURE;
    trimmed = strchr(linebuffer, ':');
    if (!trimmed)
      return EXIT_FAILURE;
    *trimmed = '\0';
    if (strcmp(linebuffer, argv[1]))
      continue; /* not the user we're looking for */
    trimmed = strchr(trimmed + 1, ':');
    if (!trimmed)
      return EXIT_FAILURE;
    trimmed = strchr(trimmed + 1, ':');
    if (!trimmed)
      return EXIT_FAILURE;
    trimmed = strchr(trimmed + 1, ':');
    if (!trimmed)
      return EXIT_FAILURE;
    uuid = trimmed;
    trimmed = strchr(trimmed + 1, ':');
    if (trimmed)
      *trimmed = '\0';
    uuid = strchr(uuid + 1, ',');
    if (!uuid)
      return EXIT_FAILURE;
    trimmed = strchr(++uuid, ',');
    if (trimmed)
      *trimmed = '\0';
    if (*uuid)
      printf("%s\n", uuid);
    return *uuid ? EXIT_SUCCESS : EXIT_FAILURE;
  }
  return EXIT_FAILURE;
}
