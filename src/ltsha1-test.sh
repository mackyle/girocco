#!/bin/sh

# License: public domain -or- http://www.wtfpl.net/txt/copying/

die() { >&2 printf 'Bail out! %s\n' "$*"; exit 1; }

testnum=0

# $1 => value to hash (may contain escapes) '-' reads stdin
# $2 => lowercase sha256 hash expected
# $3... => test name
test_sha1() {
	_val="$1"; shift
	_hash="$1"; shift
	_test="$*"
	if [ "$_val" = "-" ]; then
		_check="$(./ltsha1)"
	else
		_check="$(printf '%b' "$_val" | ./ltsha1)"
	fi ||
	die "could not run ltsha1 utility"
	_result="ok"
	[ "$_check" = "$_hash" ] || _result="not ok"
	testnum="$(( $testnum + 1 ))"
	printf '%s %u - %s\n' "$_result" "$testnum" "$_test"
}

echo '1..6'

test_sha1 \
	"abc" \
	a9993e364706816aba3e25717850c26c9cd0d89d \
	abc

test_sha1 \
	"abcdbcdecdefdefgefghfghighijhijkijkljklmklmnlmnomnopnopq" \
	84983e441c3bd26ebaae4aa1f95129e5e54670f1 \
	abcdbcdecdefdefgefghfghighijhijkijkljklmklmnlmnomnopnopq

test_sha1 \
	"123456789" \
	f7c3bc1d808e04732adf679965ccc34ca7ae3441 \
	123456789

test_sha1 \
	"The quick brown fox jumped over the lazy dog.\n" \
	bae5ed658ab3546aee12f23f36392f35dba1ebdd \
	"quick brown fox"

test_sha1 \
	- <gitblob.bin \
	e69de29bb2d1d6434b8b29ae775ad8c2e48c5391 \
	"git sha1 empty blob"

test_sha1 \
	- <gittree.bin \
	4b825dc642cb6eb9a060e54bf8d69288fbee4904 \
	"git sha1 empty tree"

exit 0
