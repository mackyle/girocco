/*

can_user_push_http.c -- check project membership in group file
Copyright (c) 2014 Kyle J. McKay.  All rights reserved.

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

*/

/*
  USAGE: can_user_push_http groupname username
  Exit code 0 for yes, non-zero for no
  If username is an explicit member of groupname returns yes
  Wrong number of or bad arguments exits with non-zero
  Directly reads compiled in GROUP_FILE location
*/

#ifndef GROUP_FILE
#error GROUP_FILE must be defined to a string which is the full pathname to\
 the /etc/group format file to read
#endif

#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#define BADCHARS ":,# \t\r\n"
#define MAX_LINE_SIZE 16384 /* includes trailing \n and \0 */
#define MAX_NAME_LEN 64

static char linebuffer[MAX_LINE_SIZE];
static char membersearch[1+MAX_NAME_LEN+1+1];

int is_valid_name(const char *name)
{
  size_t len;

  if (!name || !*name)
    return 0;
  len = strlen(name);
  if (len > MAX_NAME_LEN || strcspn(name, BADCHARS) != len)
    return 0;
  return 1;
}

int main(int argc, char *argv[])
{
  FILE *groupfile;
  size_t len;

  if (argc != 3)
    return EXIT_FAILURE;
  if (!is_valid_name(argv[1]) || !is_valid_name(argv[2]))
    return EXIT_FAILURE;

  len = strlen(argv[2]);
  membersearch[0] = ',';
  strncpy(membersearch+1, argv[2], len);
  membersearch[len+1] = ',';
  membersearch[len+2] = '\0';

  groupfile = fopen(GROUP_FILE, "r");
  if (!groupfile)
    return EXIT_FAILURE;
  while (fgets(linebuffer, sizeof(linebuffer)-1, groupfile)) {
    char *trimmed, *members;
    size_t len = strlen(linebuffer);

    if (len >= sizeof(linebuffer) - 2)
      return EXIT_FAILURE;
    if (len && linebuffer[len-1] == '\n')
      linebuffer[--len] = '\0';
    trimmed = linebuffer + strspn(linebuffer, " \t");
    if (!*trimmed || *trimmed == '#')
      continue;
    if (trimmed != linebuffer)
      return EXIT_FAILURE;
    trimmed = strchr(linebuffer, ':');
    if (!trimmed)
      return EXIT_FAILURE;
    *trimmed = '\0';
    if (strcmp(linebuffer, argv[1]))
      continue; /* not the group we're looking for */
    trimmed = strchr(trimmed + 1, ':');
    if (!trimmed)
      return EXIT_FAILURE;
    trimmed = strchr(trimmed + 1, ':');
    if (!trimmed)
      return EXIT_FAILURE;
    *trimmed = ',';
    members = trimmed;
    trimmed = strchr(trimmed + 1, ':');
    if (!trimmed)
      trimmed = linebuffer + len;
    *trimmed++ = ',';
    *trimmed = '\0';
    return strstr(members, membersearch) ? EXIT_SUCCESS : EXIT_FAILURE;
  }
  return EXIT_FAILURE;
}
