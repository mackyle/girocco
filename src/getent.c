/*

getent.c -- getent utility simulation
Copyright (c) 2013 Kyle J. McKay.  All rights reserved.

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

*/

/*
  A very simple emulation of getent passwd and getent group.  No other databases
  are supported.  Only a single key is supported.

  USAGE: getent passwd|group key
  Exit code 0 for success (and the /etc/passwd|/etc/group format line is written
  to stdout), 1 for bad command, 2 for key not found.
  Wrong number of or bad arguments exits with 1.
*/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <grp.h>
#include <pwd.h>

int main(int argc, char *argv[])
{
  const char *db;
  const char *key;
  int found = 0;

  if (argc != 3)
    return 1;

  db = argv[1];
  key = argv[2];
  if (!db || !*db || !key || !*key)
    return 1;

  if (!strcmp(db, "passwd")) {
    struct passwd *pwent = getpwnam(key);
    if (pwent) {
      found = 1;
      printf("%s:%s:%d:%d:%s:%s:%s\n", pwent->pw_name, pwent->pw_passwd,
        (int)pwent->pw_uid, (int)pwent->pw_gid, pwent->pw_gecos, pwent->pw_dir,
        pwent->pw_shell);
    }
  } else if (!strcmp(db, "group")) {
    struct group *grent = getgrnam(key);
    if (grent) {
      char **memb = grent->gr_mem;
      found = 1;
      printf("%s:%s:%d:", grent->gr_name, grent->gr_passwd, grent->gr_gid);
      if (memb) {
        int first = 1;
        for (; *memb; ++memb) {
          if (first)
            first = 0;
          else
            putchar(',');
          fputs(*memb, stdout);
        }
      }
      putchar('\n');
    }
  } else {
    return 1;
  }
  return found ? 0 : 2;
}
