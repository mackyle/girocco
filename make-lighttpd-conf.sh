#!/bin/sh

# This script uses the current values of Girocco::Config to
# convert lighttpd.conf.in into lighttpd.conf.
#
# It is run automatically by "make" or "make lighttpd.conf" but
# may be run separately if desired.

set -e

. ./shlib.sh

trap "rm -f 'lighttpd.conf.$$'" EXIT
trap 'exit 130' INT
trap 'exit 143' TERM

# Include custom configuration, if any
[ ! -e config.sh ] || [ ! -f config.sh ] || [ ! -r config.sh ] || . ./config.sh

__girocco_conf="$GIROCCO_CONF"
[ -n "$__girocco_conf" ] || __girocco_conf="Girocco::Config"
perl -I"$PWD" -M"$__girocco_conf" -MGirocco::Validator -- - lighttpd.conf.in >lighttpd.conf.$$ <<'EOT'
#line 24 "make-lighttpd-conf.sh"
use strict;
use warnings;

BEGIN { # hack to make var_online_cpus and var_getconfpath available now
	$INC{'Girocco/Config.pm'} = 1;
	require Girocco::Util;
	my $online_cpus = Girocco::Util::online_cpus();
	defined($online_cpus) && $online_cpus >= 1 or $online_cpus = 1;
	eval '$Girocco::Config::var_online_cpus = $online_cpus;';
	require Girocco::Dumper;
	my $getconfpath = Girocco::Dumper::GetConfPath();
	defined($getconfpath) && $getconfpath ne "" or $getconfpath = "/usr/bin:/bin";
	eval '$Girocco::Config::var_getconfpath = $getconfpath;';
}

my $frombegin = '# ---- BEGIN LINES TO DUPLICATE ----';
my $fromend   = '# ---- END LINES TO DUPLICATE ----';
my $tobegin   = '# ---- BEGIN DUPLICATE LINES ----';
my $toend     = '# ---- END DUPLICATE LINES ----';

open IN, '<', $ARGV[0] or die "could not open $ARGV[0] for reading: $!\n";
my $input;
{
	local $/;
	$input = <IN>;
}
close IN;
$input =~ s/^##.*(?:\n|$)//gm;
my $replifat = sub {
	my ($first, $not, $var, $lines, $last) = @_;
	no warnings;
	my $test = $var =~ /^\d+$/ ? 0+$var : eval('$Girocco::Config::'.$var);
	$not and $test = !$test;
	!$test and $lines =~ s/^/#/gm;
	return '#'.$first.$lines.'#'.$last;
};
my $resv = sub {
	my ($first, $extra, $eol) = @_;
	my $prefix = "";
	$prefix = $first if $first =~ /^[ \t]*(?:[#].*)?$/;
	$eol = $extra."\n" if $eol ne "";
	return $first.join($extra."\n".$prefix,map('(?<!\.'.$_.')',
		sort(keys(%Girocco::Config::reserved_suffixes)))).$eol;
};
my $cmteol = qr/[ \t]*(?:[#][^\n]*)?\n/;
1 while $input =~ s/^([ \t]*\@\@if\((!?)([a-zA-Z][a-zA-Z0-9_]*|\d+)\)\@\@(?>$cmteol)?)(.*?)
	((?<=\n)[ \t]*\@\@endif(?:\(\2\3\))?\@\@(?>$cmteol)?)/&$replifat($1, $2, $3, $4, $5)/gsmex;
{
	no warnings;
	$input =~ s/(\@\@([a-zA-Z][a-zA-Z0-9_]*)\@\@)/eval
		"\$Girocco::Config::$2 ne ''?\$Girocco::Config::$2:\$1"/gsex;
}
$input =~ s/^(.*)\@\@reserved\(([^)]*)\)\@\@((?>$cmteol)?)/&$resv($1, $2, $3)/gme;
if ($input =~ /(?:^|\n)$frombegin[^\n]*\n(.*)(?<=\n)$fromend/s) {
	my $dupelines = $1;
	if ($input =~ /^((?:.+\n)?$tobegin[^\n]*\n).*((?<=\n)$toend.*)$/s) {
		$input = $1 . $dupelines . $2;
	}
}
printf "%s", $input;
EOT
mv -f lighttpd.conf.$$ lighttpd.conf
